void plotNse(char *modsn=0, int synno=2762, int linno=2764, int difno=2769){
  gROOT->Clear();
  const int nfs=3;
  char files[nfs][100];
  sprintf(files[2],"/work1/pixtests/pixelmoduleqc/data/%06d_syn_thresholdscan/rootfile.root", synno);
  sprintf(files[1],"/work1/pixtests/pixelmoduleqc/data/%06d_lin_thresholdscan/rootfile.root", linno);
  sprintf(files[0],"/work1/pixtests/pixelmoduleqc/data/%06d_diff_thresholdscan/rootfile.root", difno);


  TH2F *hiNse  = new TH2F("NseMap", "Noise map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F *prNse  = new TH1F("NsePrj", "Noise histogram", 100, 1., 800.);
  TH1F *prNseD = new TH1F("NsePrjD", "Noise histogram", 100, 1., 800.);
  TH1F *prNseL = new TH1F("NsePrjDL", "Noise histogram", 100, 1., 800.);

  for (int nf = 0; nf<nfs;nf++){
    TFile f(files[nf],"");
    if(modsn==0)
      f.cd("20UPGFC0015180");
    else
      f.cd(modsn);
    gDirectory->cd("NoiseMap_0");
    TH2F *hitmp = (TH2F*) gDirectory->Get("Map");
    if(hitmp!=0){
      for(int i=1; i<=hitmp->GetNbinsX(); i++){
	for(int k=1;k<=hitmp->GetNbinsY();k++){
	  double thresh = hitmp->GetBinContent(i,k)+hiNse->GetBinContent(i,k);
	  hiNse->SetBinContent(i,k,thresh);
	  prNse->Fill(hitmp->GetBinContent(i,k));
	  if(nf<2)
	    prNseL->Fill(hitmp->GetBinContent(i,k));
	  if(nf==0)
	    prNseD->Fill(hitmp->GetBinContent(i,k));
	}
      }
    }
    f.Close();
  }

  TCanvas *can = (TCanvas*)gROOT->FindObject("nsecan");
  if(can==0) can = new TCanvas("nsecan", "Noise", 1200, 600);
  can->Clear();
  can->Divide(2, 1);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  can->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.12);
  hiNse->Draw("colz");
  hiNse->SetMinimum(1.);
  hiNse->SetMaximum(800.);
  hiNse->GetXaxis()->SetTitle("Column");
  hiNse->GetYaxis()->SetTitle("Row");
  hiNse->GetZaxis()->SetTitle("Noise (e)");
  can->cd(2);
  gPad->SetRightMargin(.12);
  gPad->SetLeftMargin(.12);
  prNse->Draw();
  prNseL->SetLineColor(2);
  prNseL->Draw("same");
  prNseD->SetLineColor(4);
  prNseD->Draw("same");
  prNse->GetXaxis()->SetTitle("Noise (e)");
  prNse->GetYaxis()->SetTitle("# pixels");
  
  TLegend *leg = new TLegend(.6, .7, .85, .9);
  leg->AddEntry(prNseD,"differ. FE", "l");
  leg->AddEntry(prNseL,"lin.+diff. FE", "l");
  leg->AddEntry(prNse,"all FE", "l");
  leg->Draw();
}
