void plotThr(char *modsn=0, int synno=2762, int linno=2764, int difno=2769){
  gROOT->Clear();
  const int nfs=3;
  char files[nfs][100];
  sprintf(files[0],"/work1/pixtests/pixelmoduleqc/data/%06d_syn_thresholdscan/rootfile.root", synno);
  sprintf(files[1],"/work1/pixtests/pixelmoduleqc/data/%06d_lin_thresholdscan/rootfile.root", linno);
  sprintf(files[2],"/work1/pixtests/pixelmoduleqc/data/%06d_diff_thresholdscan/rootfile.root", difno);


  TH2F *hiThr = new TH2F("ThrMap", "Threshold map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F *prThr = new TH1F("ThrPrj", "Threshold histogram", 100, 1., 2000.);

  for (int nf = 0; nf<nfs;nf++){
    TFile f(files[nf],"");
    if(modsn==0)
      f.cd("20UPGFC0015180");
    else
      f.cd(modsn);
    gDirectory->cd("ThresholdMap_0");
    TH2F *hitmp = (TH2F*) gDirectory->Get("Map");
    if(hitmp!=0){
      for(int i=1; i<=hitmp->GetNbinsX(); i++){
	for(int k=1;k<=hitmp->GetNbinsY();k++){
	  double thresh = hitmp->GetBinContent(i,k)+hiThr->GetBinContent(i,k);
	  hiThr->SetBinContent(i,k,thresh);
	  prThr->Fill(hitmp->GetBinContent(i,k));
	}
      }
    }
    f.Close();
  }

  TCanvas *can = (TCanvas*)gROOT->FindObject("thrcan");
  if(can==0) can = new TCanvas("thrcan", "Threshold", 1200, 600);
  can->Clear();
  can->Divide(2, 1);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  can->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.12);
  hiThr->Draw("colz");
  hiThr->SetMinimum(1.);
  hiThr->SetMaximum(2000.);
  hiThr->GetXaxis()->SetTitle("Column");
  hiThr->GetYaxis()->SetTitle("Row");
  hiThr->GetZaxis()->SetTitle("Threshold (e)");
  can->cd(2);
  gPad->SetRightMargin(.12);
  gPad->SetLeftMargin(.12);
  prThr->Draw();
  prThr->GetXaxis()->SetTitle("Threshold (e)");
  prThr->GetYaxis()->SetTitle("# pixels");

}
