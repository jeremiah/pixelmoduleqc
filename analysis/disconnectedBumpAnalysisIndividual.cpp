/**
calls disconnected bump plot macros
 */

#include "plotDisconnectedBumpsThresholdIndividual.cpp"
#include "plotDisconnectedBumpsCrosstalkIndividual.cpp"

void disconnectedBumpAnalysisIndividual(string input_dir, string chip, string output_dir){

  string syn_lo_threshold_file = input_dir+"/syn_lo_threshold.root";
  string lin_lo_threshold_file = input_dir+"/lin_lo_threshold.root";
  string diff_lo_threshold_file = input_dir+"/diff_lo_threshold.root";
  string syn_high_threshold_file = input_dir+"/syn_hi_threshold.root";
  string lin_high_threshold_file = input_dir+"/lin_hi_threshold.root";
  string diff_high_threshold_file = input_dir+"/diff_hi_threshold.root";
  string syn_crosstalk_file = input_dir+"/syn_crosstalk.root";
  string lin_crosstalk_file = input_dir+"/lin_crosstalk.root";
  string diff_crosstalk_file = input_dir+"/diff_crosstalk.root";

  TH2F* thresholdAnalysis = plotDisconnectedBumpsThresholdIndividual(syn_lo_threshold_file, lin_lo_threshold_file, diff_lo_threshold_file, syn_high_threshold_file, lin_high_threshold_file, diff_high_threshold_file, chip, output_dir);
  TH2F* crosstalkAnalysis = plotDisconnectedBumpsCrosstalkIndividual(syn_crosstalk_file, lin_crosstalk_file, diff_crosstalk_file, chip, output_dir);
  TH2F* finalConnectionMap = new TH2F("FinalConnectionMap", "Final Connection Map", 400, -0.5, 399.5, 192, -0.5, 191.50);
  
  for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
    for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
      finalConnectionMap->SetBinContent(i,k,thresholdAnalysis->GetBinContent(i,k));
    }
  }

  // (thresh or noise) OR crosstalk
  for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
    for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
      if (finalConnectionMap->GetBinContent(i,k) == 1) continue; 
      else finalConnectionMap->SetBinContent(i,k,crosstalkAnalysis->GetBinContent(i,k));
    }
  }

  // (thresh or noise) AND crosstalk
  // for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
  //   for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
  //     if ((finalConnectionMap->GetBinContent(i,k) == 1) and (crosstalkAnalysis->GetBinContent(i,k) == 1)) continue; 
  //     else finalConnectionMap->SetBinContent(i,k,0);
  //   }
  // }
  
  TCanvas* analysisCanvas = new TCanvas("AnalysisCanvas", "Final Analysis Canvas", 1200, 1200);
  gStyle->SetOptTitle(1);
  analysisCanvas->Divide(2,2);

  // these palettes are defined in the included files
  TExec *ex1 = new TExec("ex1","thresholdPal();");
  TExec *ex2 = new TExec("ex2","crosstalkPal();");
  TExec *ex3 = new TExec("ex3","occupancyPal();");
 
  analysisCanvas->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  thresholdAnalysis->Draw("colz");
  ex1->Draw();
  thresholdAnalysis->Draw("colz same");
  
  analysisCanvas->cd(2);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  crosstalkAnalysis->Draw("colz");
  ex2->Draw();
  crosstalkAnalysis->Draw("colz same");
  crosstalkAnalysis->SetMinimum(-0.01); // color empty bins
  crosstalkAnalysis->GetXaxis()->SetTitle("Column");
  crosstalkAnalysis->GetYaxis()->SetTitle("Row");
  crosstalkAnalysis->GetZaxis()->SetTitle("0 disconnected, 1 connected, 2 unknown");
  crosstalkAnalysis->GetZaxis()->SetTitleOffset(1.1);
  
  analysisCanvas->cd(3);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  finalConnectionMap->Draw("colz");
  ex2->Draw();
  finalConnectionMap->Draw("colz same");
  finalConnectionMap->SetMinimum(-0.01); // color empty bins
  finalConnectionMap->GetXaxis()->SetTitle("Column");
  finalConnectionMap->GetYaxis()->SetTitle("Row");
  finalConnectionMap->GetZaxis()->SetTitle("0 disconnected, 1 connected, 2 unknown");
  finalConnectionMap->GetZaxis()->SetTitleOffset(1.1);

  try {
    analysisCanvas->Print((output_dir+"/"+chip+"_final.png").c_str());
  }
  catch (exception e) {
    cout << "I think there is something wrong with your output directory. Skipping.\n";
  }
  assert(false); // exit the macro.......
}
