void plotToT(char *modsn=0, int scanno=2848){
  gROOT->Clear();
  char files[100];
  sprintf(files,"/work1/pixtests/pixelmoduleqc/data/%06d_std_totscan/rootfile.root", scanno);

  TH2F *hiToT  = new TH2F("ToTMap", "ToT map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F *prToT  = new TH1F("ToTPrj", "ToT histogram", 16, -0.5, 15.5);
  
  TFile f(files,"");
  if(modsn==0)
    f.cd("20UPGFC0015180");
  else
    f.cd(modsn);
  gDirectory->cd("MeanTotMap_0");

  TH2F *hitmp = (TH2F*) gDirectory->Get("Map");
  if(hitmp!=0){
    for(int i=1; i<=hitmp->GetNbinsX(); i++){
      for(int k=1;k<=hitmp->GetNbinsY();k++){
	hiToT->SetBinContent(i,k,hitmp->GetBinContent(i,k));
	prToT->Fill(hitmp->GetBinContent(i,k));
      }
    }
  }
  f.Close();

  TCanvas *can = (TCanvas*)gROOT->FindObject("nsecan");
  if(can==0) can = new TCanvas("totcan", "Time-over-threshold", 1200, 600);
  can->Clear();
  can->Divide(2, 1);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  can->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.12);
  hiToT->Draw("colz");
  hiToT->SetMinimum(0.);
  hiToT->SetMaximum(15.);
  hiToT->GetXaxis()->SetTitle("Column");
  hiToT->GetYaxis()->SetTitle("Row");
  hiToT->GetZaxis()->SetTitle("avg. ToT");
  can->cd(2);
  gPad->SetRightMargin(.12);
  gPad->SetLeftMargin(.14);
  prToT->Draw();
  prToT->GetXaxis()->SetTitle("avg. ToT");
  prToT->GetYaxis()->SetTitle("# pixels");
  
}
