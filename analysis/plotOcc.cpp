void plotOcc(char *modsn=0, double range_min=50., double range_max=150., int scanA=2797, int scanB=-1, int scanC=-1){
  gROOT->Clear();
  const int nfs=3;
  char files[nfs][100];
  int scanno[nfs]={scanA, scanB, scanC};
  for (int nf = 0; nf<nfs;nf++)
    files[nf][0]='\0';
  TSystemDirectory mydir("mydir","/work1/pixtests/pixelmoduleqc/data/");
  for(const auto&& subdir: *mydir.GetListOfFiles()){
    for (int nf = 0; nf<nfs;nf++){
      if(scanno[nf]>0 && files[nf][0]=='\0'){
	sprintf(files[0],"%06d", scanno[nf]);
	if(strncmp(subdir->GetName(), files[nf], 6)==0)
	  sprintf(files[nf],"%s/rootfile.root", subdir->GetTitle());
	else
	  files[nf][0]='\0';
      }
    }
  }
  
  TH2F *hiOcc = new TH2F("OccMap", "Occupancy map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F *prOcc = new TH1F("OccPrj", "Occupancy histogram", 100, range_min, range_max);

  for (int nf = 0; nf<nfs;nf++){
    if(strlen(files[nf])>0){
      TFile f(files[nf],"");
      if(modsn==0)
	f.cd("20UPGFC0015180");
      else
	f.cd(modsn);
      if(!gDirectory->cd("OccupancyMap"))
	gDirectory->cd("Occupancy");
      TH2F *hitmp = (TH2F*) gDirectory->Get("Map");
      if(hitmp!=0){
	for(int i=1; i<=hitmp->GetNbinsX(); i++){
	  for(int k=1;k<=hitmp->GetNbinsY();k++){
	    hiOcc->SetBinContent(i,k,hitmp->GetBinContent(i,k)+hiOcc->GetBinContent(i,k));
	  }
	}
      }
      f.Close();
    }
  }
  for(int i=1; i<=hiOcc->GetNbinsX(); i++){
    for(int k=1;k<=hiOcc->GetNbinsY();k++){
      prOcc->Fill(hiOcc->GetBinContent(i,k));
    }
  }

  TCanvas *can = (TCanvas*)gROOT->FindObject("occcan");
  if(can==0) can = new TCanvas("occcan", "Occupancy", 1200, 600);
  can->Clear();
  can->Divide(2, 1);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  can->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.12);
  hiOcc->Draw("colz");
  hiOcc->SetMinimum(range_min);
  hiOcc->SetMaximum(range_max);
  hiOcc->GetXaxis()->SetTitle("Column");
  hiOcc->GetYaxis()->SetTitle("Row");
  hiOcc->GetZaxis()->SetTitle("Hits");
  hiOcc->GetZaxis()->SetTitleOffset(1.1);
  can->cd(2);
  gPad->SetRightMargin(.12);
  gPad->SetLeftMargin(.14);
  prOcc->Draw();
  prOcc->GetXaxis()->SetTitle("Occupancy");
  prOcc->GetYaxis()->SetTitle("# pixels");

}
