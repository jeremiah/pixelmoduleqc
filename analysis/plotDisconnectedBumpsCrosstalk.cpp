/**
plots disconnected bumps from a disconnected bump crosstalk scan
disconnected bumps with no connected bumps are marked unknown
the edge of the sensor counts as a disconnected region

range: set possible occupancy values
threshold: min number of hits for a connected bump
 */

TH2F* plotDisconnectedBumpsCrosstalk(string crosstalkFile, string chip, double range_min=0., double range_max=201., int threshold=1){
  gROOT->Clear();

  // occupancy maps
  TH2F* hitMap = new TH2F("HitMap", "Occupancy map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F* hitHist = new TH1F("HitHist", "Occupancy histogram", range_max-range_min, range_min, range_max);

  // connection maps
  TH2F* connMap = new TH2F("ConnMap", "Crosstalk connection map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F* connHist = new TH1F("ConnHist", "Connection histogram", range_max-range_min, range_min, range_max);
  TH1F* connHistThresh = new TH1F("ConnHistThresh", "Connection threshold", range_max-range_min, range_min, range_max);
  THStack* connStack = new THStack("connStack","Connection Histogram;Occupancy with threshold for connection;# pixels");

  // get data
  TFile* f = new TFile(crosstalkFile.c_str());
  TDirectoryFile* data = (TDirectoryFile*) f->Get((chip+";1").c_str());
  TDirectoryFile* occupancy_data = (TDirectoryFile*) data->Get("OccupancyMap;1");
  TH2F* occMap = (TH2F*) occupancy_data->Get("Map;1");

  // fill occupancy histograms

  for(int i=1; i<=occMap->GetNbinsX(); i++){
    for(int k=1;k<=occMap->GetNbinsY();k++){
      hitMap->SetBinContent(i,k,occMap->GetBinContent(i,k));
    }
  }

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hitHist->Fill(hitMap->GetBinContent(i,k));
    }
  }

  // fill connection histograms
  // categorize as disconnected/connected/unknown
  int occupancy_array[402][194] = {}; // include a border of 0s around the sensor
  int hits = 0;

  for(int i=1; i<=occMap->GetNbinsX(); i++){
    for(int k=1;k<=occMap->GetNbinsY();k++){
      hits = occMap->GetBinContent(i,k);
      if (hits >= threshold) {
  	occupancy_array[i][k] = 1;
      }
      else {
  	occupancy_array[i][k] = 0;
      }
    }
  }

  for(int i=1; i<=occMap->GetNbinsX(); i++){
    for(int k=1;k<=occMap->GetNbinsY();k++){
      if (occupancy_array[i][k] == 1) {
  	continue;
      }
      else {
  	if (occupancy_array[i-1][k-1] == 1 or 
  	    occupancy_array[i-1][k] == 1 or
  	    occupancy_array[i-1][k+1] == 1 or
  	    occupancy_array[i][k-1] == 1 or
  	    occupancy_array[i][k+1] == 1 or
  	    occupancy_array[i+1][k-1] == 1 or
  	    occupancy_array[i+1][k] == 1 or
  	    occupancy_array[i+1][k+1] == 1) {
  	  occupancy_array[i][k] = 0;
  	}
  	else {
  	  occupancy_array[i][k] = 2;
  	}
      }
    }
  }

  for(int i=1; i<=occMap->GetNbinsX(); i++){
    for(int k=1;k<=occMap->GetNbinsY();k++){
      connMap->SetBinContent(i,k,occupancy_array[i][k]);
    }
  }

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hits = hitMap->GetBinContent(i,k);
      if (hits >= threshold) connHist->Fill(hits);
      else if (hits < threshold) connHistThresh->Fill(hits);
    }
  }

  f->Close(); // deletes everything created after opening f

  TCanvas *can = (TCanvas*)gROOT->FindObject("crosstalk");
  if(can==0) can = new TCanvas("crosstalk", "Crosstalk", 1200, 1200);
  can->Clear();
  can->Divide(2, 2);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(1);

  // plot occupancy data
  can->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.12);
  hitMap->Draw("colz");
  hitMap->SetMinimum(range_min);
  hitMap->SetMaximum(range_max);
  hitMap->GetXaxis()->SetTitle("Column");
  hitMap->GetYaxis()->SetTitle("Row");
  hitMap->GetZaxis()->SetTitle("Hits");
  hitMap->GetZaxis()->SetTitleOffset(1.1);
  can->cd(2);
  gPad->SetRightMargin(.12);
  gPad->SetLeftMargin(.14);
  hitHist->Draw();
  hitHist->GetXaxis()->SetTitle("Occupancy");
  hitHist->GetYaxis()->SetTitle("# pixels");

  // plot connection data
  can->cd(3);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.12);
  connMap->SetContour(3);
  connMap->Draw("colz");
  connMap->SetMinimum(-0.01); // color empty bins
  connMap->SetMaximum(2);
  connMap->GetXaxis()->SetTitle("Column");
  connMap->GetYaxis()->SetTitle("Row");
  connMap->GetZaxis()->SetTitle("Connection: 0 disconnected, 1 connected, 2 unknown");
  connMap->GetZaxis()->SetTitleOffset(1.1);
  can->cd(4);
  gPad->SetRightMargin(.12);
  gPad->SetLeftMargin(.14);
  connHist->SetFillColor(kBlue);
  connHistThresh->SetFillColor(kOrange+7);
  connHistThresh->SetLineColor(kOrange+7);
  connStack->Add(connHist);
  connStack->Add(connHistThresh);
  connStack->Draw();

  return connMap;
}
