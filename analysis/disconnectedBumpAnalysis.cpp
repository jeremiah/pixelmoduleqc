/**
calls disconnected bump plot macros
 */

#include "plotDisconnectedBumpsThreshold.cpp"
#include "plotDisconnectedBumpsCrosstalk.cpp"

void disconnectedBumpAnalysis(string lo_threshold_file, string high_threshold_file, string crosstalk_file, string chip){
  TH2F* thresholdAnalysis = plotDisconnectedBumpsThreshold(lo_threshold_file, high_threshold_file, chip);
  TH2F* crosstalkAnalysis = plotDisconnectedBumpsCrosstalk(crosstalk_file, chip);
  TH2F* finalConnectionMap = new TH2F("FinalConnectionMap", "Final Connection Map", 400, -0.5, 399.5, 192, -0.5, 191.50);
  
  for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
    for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
      finalConnectionMap->SetBinContent(i,k,thresholdAnalysis->GetBinContent(i,k));
    }
  }

  // (thresh or noise) OR crosstalk
  for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
    for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
      if (finalConnectionMap->GetBinContent(i,k) == 1) continue; 
      else finalConnectionMap->SetBinContent(i,k,crosstalkAnalysis->GetBinContent(i,k));
    }
  }

  // (thresh or noise) AND crosstalk
  // for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
  //   for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
  //     if ((finalConnectionMap->GetBinContent(i,k) == 1) and (crosstalkAnalysis->GetBinContent(i,k) == 1)) continue; 
  //     else finalConnectionMap->SetBinContent(i,k,0);
  //   }
  // }
  
  TCanvas* analysisCanvas = new TCanvas("AnalysisCanvas", "Final Analysis Canvas", 1200, 1200);
  gStyle->SetOptTitle(1);
  analysisCanvas->Divide(2,2);
  analysisCanvas->cd(1);
  thresholdAnalysis->Draw("colz");
  analysisCanvas->cd(2);
  crosstalkAnalysis->Draw("colz");
  crosstalkAnalysis->SetMinimum(-0.01); // color empty bins
  crosstalkAnalysis->SetMaximum(2);
  crosstalkAnalysis->GetXaxis()->SetTitle("Column");
  crosstalkAnalysis->GetYaxis()->SetTitle("Row");
  crosstalkAnalysis->GetZaxis()->SetTitle("Connection: 0 disconnected, 1 connected, 2 unknown");
  crosstalkAnalysis->GetZaxis()->SetTitleOffset(1.1);
  analysisCanvas->cd(3);
  finalConnectionMap->Draw("colz");
  finalConnectionMap->SetMinimum(-0.01); // color empty bins
  finalConnectionMap->SetMaximum(2);
  finalConnectionMap->GetXaxis()->SetTitle("Column");
  finalConnectionMap->GetYaxis()->SetTitle("Row");
  finalConnectionMap->GetZaxis()->SetTitle("Connection: 0 disconnected, 1 connected, 2 unknown");
  finalConnectionMap->GetZaxis()->SetTitleOffset(1.1);
}


