/**
plots disconnected bumps from 2 threshold scans
 */

TH2F* plotDisconnectedBumpsThreshold(string loFile, string hiFile, string chip){
  gROOT->Clear();
  // variables
  int synBoundary = 128; // flavor x boundaries
  int linBoundary = 264;
  int diffBoundary = 400;
  
  int xrange = 400; // sensor dimensions
  int yrange = 192;

  int thresholdCutSynLow = -25; // cut thresholds from BDAQ
  int thresholdCutSynHigh = 30;
  int thresholdCutLinLow = -50;
  int thresholdCutLinHigh = 60;
  int thresholdCutDiffLow = -150;
  int thresholdCutDiffHigh = 80;
  int noiseCutSynLow = -40;
  int noiseCutSynHigh = 40;
  int noiseCutLinLow = -25;
  int noiseCutLinHigh = 25;
  int noiseCutDiffLow = -200;
  int noiseCutDiffHigh = 200;

  // shift plots
  double shiftLower = -1000.; // to make the shift plot easier to change
  double shiftUpper = 1000.;
  TH1F* synThresholdShift = new TH1F("SynThresholdShift", "Threshold Shift of Syn flavor", 100, shiftLower, shiftUpper);
  TH1F* linThresholdShift = new TH1F("LinThresholdShift", "Threshold Shift of Lin flavor", 100, shiftLower, shiftUpper);
  TH1F* diffThresholdShift = new TH1F("DiffThresholdShift", "Threshold Shift of Diff flavor", 100, shiftLower, shiftUpper);
  THStack* shiftThresholdStack = new THStack("shiftThresholdStack","Threshold, Red: Diff, Green: Lin, Blue: Sync");

  TH1F* synNoiseShift = new TH1F("SynNoiseShift", "Threhsold Shift of Syn flavor", 100, shiftLower, shiftUpper);
  TH1F* linNoiseShift = new TH1F("LinNoiseShift", "Noise Shift of Lin flavor", 100, shiftLower, shiftUpper);
  TH1F* diffNoiseShift = new TH1F("DiffNoiseShift", "Noise Shift of Diff flavor", 100, shiftLower, shiftUpper);
  THStack* shiftNoiseStack = new THStack("shiftNoiseStack","Noise, Red: Diff, Green: Lin, Blue: Sync");

  // connection plots
  TH2F* pixelStatusMapThreshold = new TH2F("PixelStatusMapThreshold", "Threshold/Noise Shift Connection Map", 400, -0.5, 399.5, 192, -0.5, 191.5);

  // get data
  TFile* flo = new TFile(loFile.c_str());
  TDirectoryFile* loData = (TDirectoryFile*) flo->Get((chip+";1").c_str());
  TDirectoryFile* loThresholdData = (TDirectoryFile*) loData->Get("ThresholdMap_0;1");
  TH2F* loThresholdSyn = (TH2F*) loThresholdData->Get("Map_Syn;1");
  TH2F* loThresholdLin = (TH2F*) loThresholdData->Get("Map_Lin;1");
  TH2F* loThresholdDiff = (TH2F*) loThresholdData->Get("Map_Diff;1");
  TDirectoryFile* loNoiseData = (TDirectoryFile*) loData->Get("NoiseMap_0;1");
  TH2F* loNoiseSyn = (TH2F*) loNoiseData->Get("Map_Syn;1");
  TH2F* loNoiseLin = (TH2F*) loNoiseData->Get("Map_Lin;1");
  TH2F* loNoiseDiff = (TH2F*) loNoiseData->Get("Map_Diff;1");

  TFile* fhi = new TFile(hiFile.c_str());
  TDirectoryFile* hiData = (TDirectoryFile*) fhi->Get((chip+";1").c_str());
  TDirectoryFile* hiThresholdData = (TDirectoryFile*) hiData->Get("ThresholdMap_0;1");
  TH2F* hiThresholdSyn = (TH2F*) hiThresholdData->Get("Map_Syn;1");
  TH2F* hiThresholdLin = (TH2F*) hiThresholdData->Get("Map_Lin;1");
  TH2F* hiThresholdDiff = (TH2F*) hiThresholdData->Get("Map_Diff");
  TDirectoryFile* hiNoiseData = (TDirectoryFile*) hiData->Get("NoiseMap_0;1");
  TH2F* hiNoiseSyn = (TH2F*) hiNoiseData->Get("Map_Syn;1");
  TH2F* hiNoiseLin = (TH2F*) hiNoiseData->Get("Map_Lin;1");
  TH2F* hiNoiseDiff = (TH2F*) hiNoiseData->Get("Map_Diff");

  // Fill shift plots
  hiThresholdSyn -> Add(loThresholdSyn, -1);
  hiThresholdLin -> Add(loThresholdLin, -1);
  hiThresholdDiff -> Add(loThresholdDiff, -1);

  for(int i=1; i<=xrange; i++){
    for(int k=1; k<=yrange;k++){
      if (i <= synBoundary) {
	synThresholdShift -> Fill(hiThresholdSyn->GetBinContent(i,k));
      }
      else if (i <= linBoundary) {
	linThresholdShift -> Fill(hiThresholdLin->GetBinContent(i,k));
      }
      else if (i <= diffBoundary) {
	diffThresholdShift -> Fill(hiThresholdDiff->GetBinContent(i,k));
      }
    }
  }

  hiNoiseSyn -> Add(loNoiseSyn, -1);
  hiNoiseLin -> Add(loNoiseLin, -1);
  hiNoiseDiff -> Add(loNoiseDiff, -1);

  for(int i=1; i<=xrange; i++){
    for(int k=1; k<=yrange;k++){
      if (i <= synBoundary) {
	synNoiseShift -> Fill(hiNoiseSyn->GetBinContent(i,k));
      }
      else if (i <= linBoundary) {
	linNoiseShift -> Fill(hiNoiseLin->GetBinContent(i,k));
      }
      else if (i <= diffBoundary) {
	diffNoiseShift -> Fill(hiNoiseDiff->GetBinContent(i,k));
      }
    }
  }

  // categorize pixels
  for(int i=1; i<=xrange; i++){
    for(int k=1; k<=yrange; k++){
      if (i <= synBoundary) {
	if ((hiThresholdSyn->GetBinContent(i,k) < thresholdCutSynLow) or
	    (hiThresholdSyn->GetBinContent(i,k) > thresholdCutSynHigh) or
	    (hiNoiseSyn->GetBinContent(i,k) < noiseCutSynLow) or
	    (hiNoiseSyn->GetBinContent(i,k) > noiseCutSynHigh)) {
	  pixelStatusMapThreshold->SetBinContent(i,k,1);
	}
	else pixelStatusMapThreshold->SetBinContent(i,k,0.01); 
      }
      else if (i <= linBoundary) {
	if ((hiThresholdLin->GetBinContent(i,k) < thresholdCutLinLow) or
	    (hiThresholdLin->GetBinContent(i,k) > thresholdCutLinHigh) or
	    (hiNoiseLin->GetBinContent(i,k) < noiseCutLinLow) or
	    (hiNoiseLin->GetBinContent(i,k) > noiseCutLinHigh)) {
	  pixelStatusMapThreshold->SetBinContent(i,k,1);
	}
	else pixelStatusMapThreshold->SetBinContent(i,k,0.01);    
      }
      else if (i <= diffBoundary) {
	if ((hiThresholdDiff->GetBinContent(i,k) < thresholdCutDiffLow) or
	    (hiThresholdDiff->GetBinContent(i,k) > thresholdCutDiffHigh) or
	    (hiNoiseDiff->GetBinContent(i,k) < noiseCutDiffLow) or
	    (hiNoiseDiff->GetBinContent(i,k) > noiseCutDiffHigh)) {
	  pixelStatusMapThreshold->SetBinContent(i,k,1);
	}
	else pixelStatusMapThreshold->SetBinContent(i,k,0.01); 
      }
    }
  }

  flo->Close(); // deletes everything created after flo and before fhi
  fhi->Close(); // deletes everything created after fhi

  TCanvas *can = (TCanvas*)gROOT->FindObject("thrcan");
  if(can==0) can = new TCanvas("thrcan", "Threshold", 1200, 1200);
  can->Clear();
  can->Divide(2, 2);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(1);

  can->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.12);
  pixelStatusMapThreshold->Draw("colz");
  can->cd(2);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.12);
  can->cd(3);
  synNoiseShift->SetFillColor(kBlue);
  linNoiseShift->SetFillColor(kGreen);
  diffNoiseShift->SetFillColor(kRed);
  shiftNoiseStack->Add(synNoiseShift);
  shiftNoiseStack->Add(linNoiseShift);
  shiftNoiseStack->Add(diffNoiseShift);
  shiftNoiseStack->Draw("nostack");
  can->cd(4);
  synThresholdShift->SetFillColor(kBlue);
  linThresholdShift->SetFillColor(kGreen);
  diffThresholdShift->SetFillColor(kRed);
  shiftThresholdStack->Add(synThresholdShift);
  shiftThresholdStack->Add(linThresholdShift);
  shiftThresholdStack->Add(diffThresholdShift);
  shiftThresholdStack->Draw("nostack");

  return pixelStatusMapThreshold;
}
