#include <nlohmann/json.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TFile.h>

int main(int argc, char **argv){
  if(argc!=2){
    printf("Usage: jsonToRootIV <module json file> <baremodule json file\n");
    return -1;
  }

  // read json data
  nlohmann::json ivdata;
  std::ifstream i(argv[1]);
  i >> ivdata;
  std::vector<double> curr  = ivdata["values"]["Current"];
  std::vector<double> volts = ivdata["values"]["Voltage"];

  if( curr.size() != volts.size() ) {
    printf("Current and voltage vectors have different size!\n");
    return -2;
  }

  TGraphErrors *gr = new TGraphErrors;
  double lastV = 99999.;
  double npt=0., sumI=0., sumIsq=0.;
  for(uint i=0; i<curr.size();i++){
    if(lastV!=volts.at(i)){
      if(i>0 && npt>1){
	gr->SetPoint(gr->GetN(), (-1.)*lastV, (-1.)*sumI/npt*1.e6);
	gr->SetPointError(gr->GetN()-1, 0.05, sqrt((sumIsq/npt-sumI*sumI/npt/npt)/(npt-1))*1.e6);
      }
      sumI = curr.at(i);
      sumIsq = curr.at(i)*curr.at(i);
      npt = 1.;
    } else {
      sumI += curr.at(i);
      sumIsq += curr.at(i)*curr.at(i);
      npt += 1.;
    }
    lastV = volts.at(i);
  }
  if(npt>1){
    gr->SetPoint(gr->GetN(), (-1.)*lastV, (-1.)*sumI/npt*1.e6);
    gr->SetPointError(gr->GetN()-1, 0.05, sqrt((sumIsq/npt-sumI*sumI/npt/npt)/(npt-1))*1.e6);
  }

  remove("output.root");
  TFile f("output.root","UPDATE");
  gr->Write("modivgr");
  f.Close();

  return 0;
}
