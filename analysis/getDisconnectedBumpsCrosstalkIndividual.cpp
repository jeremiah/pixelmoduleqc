/**
get disconnected bumps from a disconnected bump crosstalk scan
disconnected bumps with no connected bumps are marked unknown
the edge of the sensor counts as a disconnected region
frontends scanned individually

range: set possible occupancy values
threshold: min number of hits for a connected bump
 */

TH2F* getDisconnectedBumpsCrosstalkIndividual(string synFile, string linFile, string diffFile, string chip){
  gROOT->Clear();
  double range_min=0.;
  double range_max=120.;
  int threshold=1;
  
  // occupancy maps - will stay open after TFiles are closed
  TH2F* hitMap = new TH2F("HitMap", "Occupancy map", 400, -0.5, 399.5, 192, -0.5, 191.5);

  // connection maps
  TH2F* connMap = new TH2F("ConnMap", "Crosstalk connection map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F* connHist = new TH1F("ConnHist", "Connection histogram", range_max-range_min, range_min, range_max);
  TH1F* connHistThresh = new TH1F("ConnHistThresh", "Connection threshold", range_max-range_min, range_min, range_max);
  THStack* connStack = new THStack("connStack","Connection Histogram;Occupancy with threshold for connection;# pixels");

  // get data
  TFile* fSyn = new TFile(synFile.c_str());
  TDirectoryFile* dataSyn = (TDirectoryFile*) fSyn->Get(chip.c_str());
  TDirectoryFile* occupancyDataSyn = (TDirectoryFile*) dataSyn->Get("OccupancyMap;1");
  TH2F* occMapSyn = (TH2F*) occupancyDataSyn->Get("Map;1");
  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hitMap->SetBinContent(i,k,occMapSyn->GetBinContent(i,k)+hitMap->GetBinContent(i,k));
    }
  }
  fSyn->Close();

  TFile* fLin = new TFile(linFile.c_str());
  TDirectoryFile* dataLin = (TDirectoryFile*) fLin->Get(chip.c_str());
  TDirectoryFile* occupancyDataLin = (TDirectoryFile*) dataLin->Get("OccupancyMap;1");
  TH2F* occMapLin = (TH2F*) occupancyDataLin->Get("Map;1");
  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hitMap->SetBinContent(i,k,occMapLin->GetBinContent(i,k)+hitMap->GetBinContent(i,k));
    }
  }
  fLin->Close();

  TFile* fDiff = new TFile(diffFile.c_str());
  TDirectoryFile* dataDiff = (TDirectoryFile*) fDiff->Get(chip.c_str());
  TDirectoryFile* occupancyDataDiff = (TDirectoryFile*) dataDiff->Get("OccupancyMap;1");
  TH2F* occMapDiff = (TH2F*) occupancyDataDiff->Get("Map;1");
  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hitMap->SetBinContent(i,k,occMapDiff->GetBinContent(i,k)+hitMap->GetBinContent(i,k));
    }
  }
  fDiff->Close();

  // fill connection histograms
  // categorize as disconnected/connected/unknown
  int occupancy_array[402][194] = {}; // include a border of 0s around the sensor
  int hits = 0;

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hits = hitMap->GetBinContent(i,k);
      if (hits >= threshold) {
  	occupancy_array[i][k] = 1;
      }
      else {
  	occupancy_array[i][k] = 0;
      }
    }
  }

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      if (occupancy_array[i][k] == 1) {
  	continue;
      }
      else {
  	if (occupancy_array[i-1][k-1] == 1 or 
  	    occupancy_array[i-1][k] == 1 or
  	    occupancy_array[i-1][k+1] == 1 or
  	    occupancy_array[i][k-1] == 1 or
  	    occupancy_array[i][k+1] == 1 or
  	    occupancy_array[i+1][k-1] == 1 or
  	    occupancy_array[i+1][k] == 1 or
  	    occupancy_array[i+1][k+1] == 1) {
  	  occupancy_array[i][k] = 0;
  	}
  	else {
  	  occupancy_array[i][k] = 2;
  	}
      }
    }
  }

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      connMap->SetBinContent(i,k,occupancy_array[i][k]);
    }
  }

  return connMap;
}
