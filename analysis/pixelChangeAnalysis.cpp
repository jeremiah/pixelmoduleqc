// rough draft comparison of pixels before and after cycling

// these files are the same as the plotting ones, except they only return files and don't plot
// it might be better to add another argument to the plotting files: 0 = only return file, 1 = plot as well
// then it is easier to maintain the different files instead of having this separate set of them
#include "getDisconnectedBumpsCrosstalkIndividual.cpp"
#include "getDisconnectedBumpsThresholdIndividual.cpp"

// this is basically the same as disconnectedBumpAnalysisIndividual.cpp, except it returns the final TH2F and does not crash root to exit
TH2F* getFinalConnectionMap(string path, string chip){
  string syn_lo_threshold_file = path+"/syn_lo_threshold.root";
  string lin_lo_threshold_file = path+"/lin_lo_threshold.root";
  string diff_lo_threshold_file = path+"/diff_lo_threshold.root";
  string syn_high_threshold_file = path+"/syn_hi_threshold.root";
  string lin_high_threshold_file = path+"/lin_hi_threshold.root";
  string diff_high_threshold_file = path+"/diff_hi_threshold.root";
  string syn_crosstalk_file = path+"/syn_crosstalk.root";
  string lin_crosstalk_file = path+"/lin_crosstalk.root";
  string diff_crosstalk_file = path+"/diff_crosstalk.root";
  
  TH2F* thresholdAnalysis = getDisconnectedBumpsThresholdIndividual(syn_lo_threshold_file, lin_lo_threshold_file, diff_lo_threshold_file, syn_high_threshold_file, lin_high_threshold_file, diff_high_threshold_file, chip);
  TH2F* crosstalkAnalysis = getDisconnectedBumpsCrosstalkIndividual(syn_crosstalk_file, lin_crosstalk_file, diff_crosstalk_file, chip);
  TH2F* finalConnectionMap = new TH2F("FinalConnectionMap", "Final Connection Map", 400, -0.5, 399.5, 192, -0.5, 191.50);
  
  for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
    for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
      finalConnectionMap->SetBinContent(i,k,thresholdAnalysis->GetBinContent(i,k));
    }
  }

  // (thresh or noise) OR crosstalk
  for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
    for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
      if (finalConnectionMap->GetBinContent(i,k) == 1) continue; 
      else finalConnectionMap->SetBinContent(i,k,crosstalkAnalysis->GetBinContent(i,k));
    }
  }

  // (thresh or noise) AND crosstalk
  // for(int i=1; i<=finalConnectionMap->GetNbinsX(); i++){
  //   for(int k=1;k<=finalConnectionMap->GetNbinsY();k++){
  //     if ((finalConnectionMap->GetBinContent(i,k) == 1) and (crosstalkAnalysis->GetBinContent(i,k) == 1)) continue; 
  //     else finalConnectionMap->SetBinContent(i,k,0);
  //   }
  // }

  return finalConnectionMap;
}

void pixelChangeAnalysis(string before_testing_dir, string after_testing_dir, string chip, string output_dir) {
  TH2F* before = getFinalConnectionMap(before_testing_dir, chip);
  TH2F* after = getFinalConnectionMap(after_testing_dir, chip);
  TH2F* change = new TH2F("change", "change from before to after", 400, -0.5, 399.5, 192, -0.5, 191.50);
  for(int i=1; i<=after->GetNbinsX(); i++){
    for(int k=1;k<=after->GetNbinsY();k++){
      if (after->GetBinContent(i,k) == before->GetBinContent(i,k)) change->SetBinContent(i,k,0); // no change
      else if ((after->GetBinContent(i,k) == 2) and (before->GetBinContent(i,k) == 1)) change->SetBinContent(i,k,-1); // connected -> unknown
      else if ((after->GetBinContent(i,k) == 2) and (before->GetBinContent(i,k) == 0)) change->SetBinContent(i,k,0); // disconnected -> unknown
      else if ((after->GetBinContent(i,k) == 1) and (before->GetBinContent(i,k) == 2)) change->SetBinContent(i,k,1); // unknown -> connected
      else if ((after->GetBinContent(i,k) == 1) and (before->GetBinContent(i,k) == 0)) change->SetBinContent(i,k,1); // disconnected -> connected
      else if ((after->GetBinContent(i,k) == 0) and (before->GetBinContent(i,k) == 2)) change->SetBinContent(i,k,0); // unknown -> disconnected
      else if ((after->GetBinContent(i,k) == 0) and (before->GetBinContent(i,k) == 1)) change->SetBinContent(i,k,-1); // connected -> disconnected
    }
  }

  TCanvas* diffCanvas = new TCanvas("diffCanvas", "After minus Before 0x3659", 1000, 1000);
  Int_t diffPalette[3];
  diffPalette[0] = kBlue;
  diffPalette[1] = kWhite;
  diffPalette[2] = kOrange+7;

  gStyle->SetOptTitle(1);
  gStyle->SetOptStat(0);
  gStyle->SetPalette(3,diffPalette);
  diffCanvas->SetRightMargin(0.2);
  change->GetZaxis()->SetTitle("conn->disc: -1, no change: 0, disconn->conn: 1");
  change->GetZaxis()->SetTitleOffset(1.5);
  //change->GetZaxis()->SetTitleOffset(0.35);
  change->Draw("colz");
  diffCanvas->Print((output_dir+"/"+chip+"_pixel_status_change.png").c_str());
}
