void plotIV(char *fname=0){
  gROOT->Clear();

  char command[100];
  if(fname!=0)
    sprintf(command, "./jsonToRootIV ../results/%s", fname);
  else
    sprintf(command, "./jsonToRootIV ../results/20UPGM20021168_sensorIV_20C.json");
  

  gSystem->Exec(command);
  
  TFile f("output.root");
  TGraph *gr_in = (TGraph*)f.Get("modivgr");
  TGraph *gr=0;
  if(gr_in!=0){
    gROOT->cd();    
    gr = new TGraph(gr_in->GetN(), gr_in->GetX(), gr_in->GetY());
  }
  f.Close();
  remove("output.root");
  if(gr==0) return;
  gr->SetMarkerStyle(20);

  const uint bntp = 41;
  double bareV[bntp]={0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100,105,110,115,120,125,130,135,140,145,150,155,160,165,170,175,180,185,190,195,200};

  double bareI[bntp]={0.00245499,0.0325099,0.0358952,0.0374599,0.0387708,0.0402158,0.0414819,0.0424336,0.0438393,0.0449068,0.0450282,0.0451871,0.0456712,0.0462178,0.0466977,0.0471372,0.0476384,0.0479825,0.0485335,0.0491014,0.0496182,0.05024,0.0504811,0.0513468,0.0518197,0.0523738,0.0526805,0.0533087,0.0539115,0.0544452,0.0549852,0.0556098,0.0564561,0.0566295,0.0577285,0.0581887,0.0588834,0.0598033,0.0599584,0.0610306,0.0620436};

  TGraph *grB = new TGraph;
  double Tbare = 21.7, Tmod=20.;
  for(uint i=0;i<bntp;i++){
    grB->SetPoint(i, bareV[i], bareI[i]*(273.15+Tmod)*(273.15+Tmod)/(273.15+Tbare)/(273.15+Tbare)*TMath::Exp(1.22*(Tmod-Tbare)/2./8.617e-5/(273.15+Tmod)/(273.15+Tbare)));
  }
  grB->SetMarkerStyle(24);

  TCanvas *can = (TCanvas*)gROOT->FindObject("ivcan");
  if(can==0) can = new TCanvas("ivcan", "IV scan", 800, 500);
  can->Clear();
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  can->cd();

  TMultiGraph *mg = new TMultiGraph;
  mg->Add(gr);
  mg->Add(grB);
  mg->Draw("AP");
  mg->GetXaxis()->SetTitle("voltage (V)");
  mg->GetYaxis()->SetTitle("current (#muA)");

  TLegend *leg = new TLegend(.65, .15, .85, .3);
  leg->AddEntry(gr, "assembled (20^{o}C)", "p");
  leg->AddEntry(grB, "bare (corr. to 20^{o}C)", "p");
  leg->Draw();
}
