void plotVI(char *fname=0){
  gROOT->Clear();

  char command[100];
  if(fname!=0)
    sprintf(command, "./jsonToRootVI ../results/%s", fname);
  else
    sprintf(command, "./jsonToRootVI ../results/20UPGM20021168_SLDO-VI-4.8_20C_0.json");

  gSystem->Exec(command);
  
  TFile f("output.root");
  TGraph *gr_in = (TGraph*)f.Get("vigr");
  TGraph *gr=0;
  if(gr_in!=0){
    gROOT->cd();    
    gr = new TGraph(gr_in->GetN(), gr_in->GetX(), gr_in->GetY());
  }
  f.Close();
  remove("output.root");
  if(gr==0) return;
  gr->SetMarkerStyle(20);

  TCanvas *can = (TCanvas*)gROOT->FindObject("vican");
  if(can==0) can = new TCanvas("vican", "VI scan", 800, 500);
  can->Clear();
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  can->cd();

  gr->Draw("AP");
  gr->GetXaxis()->SetTitle("current (A)");
  gr->GetYaxis()->SetTitle("voltage (V)");
}
