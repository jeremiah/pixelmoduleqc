#include <nlohmann/json.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <TGraph.h>
#include <TFile.h>

int main(int argc, char **argv){
  if(argc!=2){
    printf("Usage: jsonToRootVI <json file>\n");
    return -1;
  }

  // read json data
  nlohmann::json ivdata;
  std::ifstream i(argv[1]);
  i >> ivdata;
  std::vector<double> curr  = ivdata["values"]["Current"];
  std::vector<double> volts = ivdata["values"]["Voltage"];

  if( curr.size() != volts.size() ) {
    printf("Current and voltage vectors have different size!\n");
    return -2;
  }

  TGraph *gr = new TGraph;
  for(uint i=0; i<curr.size();i++){
    gr->SetPoint(i, curr.at(i), volts.at(i));
  }

  remove("output.root");
  TFile f("output.root","UPDATE");
  gr->Write("vigr");
  f.Close();

  return 0;
}
