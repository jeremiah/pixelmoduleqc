/**
plots disconnected bumps from a disconnected bump crosstalk scan
disconnected bumps with no connected bumps are marked unknown
the edge of the sensor counts as a disconnected region
frontends scanned individually

range: set possible occupancy values
threshold: min number of hits for a connected bump
 */

TH2F* plotDisconnectedBumpsCrosstalkIndividual(string synFile, string linFile, string diffFile, string chip, string output_dir){
  gROOT->Clear();
  double range_min=0.;
  double range_max=120.;
  int threshold=1;
  
  // occupancy maps - will stay open after TFiles are closed
  TH2F* hitMap = new TH2F("HitMap", "Occupancy map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F* hitHist = new TH1F("HitHist", "Occupancy histogram", range_max-range_min, range_min, range_max);

  // connection maps
  TH2F* connMap = new TH2F("ConnMap", "Crosstalk connection map", 400, -0.5, 399.5, 192, -0.5, 191.5);
  TH1F* connHist = new TH1F("ConnHist", "Connection histogram", range_max-range_min, range_min, range_max);
  TH1F* connHistThresh = new TH1F("ConnHistThresh", "Connection threshold", range_max-range_min, range_min, range_max);
  THStack* connStack = new THStack("connStack","Connection Histogram;Occupancy with threshold for connection;# pixels");

  // get data
  TFile* fSyn = new TFile(synFile.c_str());
  TDirectoryFile* dataSyn = (TDirectoryFile*) fSyn->Get(chip.c_str());
  TDirectoryFile* occupancyDataSyn = (TDirectoryFile*) dataSyn->Get("OccupancyMap;1");
  TH2F* occMapSyn = (TH2F*) occupancyDataSyn->Get("Map;1");
  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hitMap->SetBinContent(i,k,occMapSyn->GetBinContent(i,k)+hitMap->GetBinContent(i,k));
    }
  }
  fSyn->Close();

  TFile* fLin = new TFile(linFile.c_str());
  TDirectoryFile* dataLin = (TDirectoryFile*) fLin->Get(chip.c_str());
  TDirectoryFile* occupancyDataLin = (TDirectoryFile*) dataLin->Get("OccupancyMap;1");
  TH2F* occMapLin = (TH2F*) occupancyDataLin->Get("Map;1");
  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hitMap->SetBinContent(i,k,occMapLin->GetBinContent(i,k)+hitMap->GetBinContent(i,k));
    }
  }
  fLin->Close();

  TFile* fDiff = new TFile(diffFile.c_str());
  TDirectoryFile* dataDiff = (TDirectoryFile*) fDiff->Get(chip.c_str());
  TDirectoryFile* occupancyDataDiff = (TDirectoryFile*) dataDiff->Get("OccupancyMap;1");
  TH2F* occMapDiff = (TH2F*) occupancyDataDiff->Get("Map;1");
  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hitMap->SetBinContent(i,k,occMapDiff->GetBinContent(i,k)+hitMap->GetBinContent(i,k));
    }
  }
  fDiff->Close();

  // fill occupancy histogram
  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hitHist->Fill(hitMap->GetBinContent(i,k));
    }
  }

  // fill connection histograms
  // categorize as disconnected/connected/unknown
  int occupancy_array[402][194] = {}; // include a border of 0s around the sensor
  int hits = 0;

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hits = hitMap->GetBinContent(i,k);
      if (hits >= threshold) {
  	occupancy_array[i][k] = 1;
      }
      else {
  	occupancy_array[i][k] = 0;
      }
    }
  }

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      if (occupancy_array[i][k] == 1) {
  	continue;
      }
      else {
  	if (occupancy_array[i-1][k-1] == 1 or 
  	    occupancy_array[i-1][k] == 1 or
  	    occupancy_array[i-1][k+1] == 1 or
  	    occupancy_array[i][k-1] == 1 or
  	    occupancy_array[i][k+1] == 1 or
  	    occupancy_array[i+1][k-1] == 1 or
  	    occupancy_array[i+1][k] == 1 or
  	    occupancy_array[i+1][k+1] == 1) {
  	  occupancy_array[i][k] = 0;
  	}
  	else {
  	  occupancy_array[i][k] = 2;
  	}
      }
    }
  }

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      connMap->SetBinContent(i,k,occupancy_array[i][k]);
    }
  }

  for(int i=1; i<=hitMap->GetNbinsX(); i++){
    for(int k=1;k<=hitMap->GetNbinsY();k++){
      hits = hitMap->GetBinContent(i,k);
      if (hits >= threshold) connHist->Fill(hits);
      else if (hits < threshold) connHistThresh->Fill(hits);
    }
  }

  TCanvas *crosstalkCanvas = (TCanvas*)gROOT->FindObject("crosstalkCanvas");
  if(crosstalkCanvas==0) crosstalkCanvas = new TCanvas("crosstalkCanvas", "Crosstalk", 1200, 1200);
  crosstalkCanvas->Clear();
  crosstalkCanvas->Divide(2, 2);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(1);

  TExec *ex2 = new TExec("ex2","crosstalkPal();");
  TExec *ex3 = new TExec("ex3","occupancyPal();");

  // plot occupancy data
  crosstalkCanvas->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  hitMap->Draw("colz");
  ex3->Draw();
  hitMap->Draw("colz same");
  hitMap->SetMinimum(range_min);
  hitMap->SetMaximum(range_max);
  hitMap->SetMinimum(-0.01); // color empty bins
  hitMap->GetXaxis()->SetTitle("Column");
  hitMap->GetYaxis()->SetTitle("Row");
  hitMap->GetZaxis()->SetTitle("Hits");
  hitMap->GetZaxis()->SetTitleOffset(1.1);

  crosstalkCanvas->cd(2);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  hitHist->Draw();
  hitHist->GetXaxis()->SetTitle("Occupancy");
  hitHist->GetYaxis()->SetTitle("# pixels");

  // plot connection data
  crosstalkCanvas->cd(3);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  connMap->Draw("colz");
  ex2->Draw();
  connMap->Draw("colz same");
  connMap->SetMinimum(-0.01); // color empty bins
  connMap->GetXaxis()->SetTitle("Column");
  connMap->GetYaxis()->SetTitle("Row");
  connMap->GetZaxis()->SetTitle("0 disconnected, 1 connected, 2 unknown");
  connMap->GetZaxis()->SetTitleOffset(1.1);

  crosstalkCanvas->cd(4);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  connHist->SetFillColor(kBlue);
  connHistThresh->SetFillColor(kOrange+7);
  connHistThresh->SetLineColor(kOrange+7);
  connStack->Add(connHist);
  connStack->Add(connHistThresh);
  connStack->Draw();
  try {
    crosstalkCanvas->Print((output_dir+"/"+chip+"_crosstalk.png").c_str());
  }
  catch (exception e) {
    cout << "Crosstalk print failed.\n.";
  }
  return connMap;
}

void crosstalkPal() { // crosstalk palette
  Int_t crosstalkPalette[3];
  crosstalkPalette[0] = kOrange;
  crosstalkPalette[1] = kMagenta+3;
  crosstalkPalette[2] = kWhite;
  gStyle->SetPalette(3,crosstalkPalette);
}

void occupancyPal() { // occupancy palette
  gStyle->SetPalette(kSunset);
  TColor::InvertPalette();
}
