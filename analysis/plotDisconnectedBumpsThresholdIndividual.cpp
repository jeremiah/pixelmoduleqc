/**
plots disconnected bumps from 2 threshold scans
 */

TH2F* plotDisconnectedBumpsThresholdIndividual(string synLoFile, string linLoFile, string diffLoFile, string synHiFile, string linHiFile, string diffHiFile, string chip, string output_dir){
  gROOT->Clear();
  // variables
  int synBoundary = 128; // flavor x boundaries
  int linBoundary = 264;
  int diffBoundary = 400;
  
  int xrange = 400; // sensor dimensions
  int yrange = 192;

  int thresholdCutSynLow = -25; // cut thresholds from BDAQ
  int thresholdCutSynHigh = 30;
  int thresholdCutLinLow = -50;
  int thresholdCutLinHigh = 60;
  int thresholdCutDiffLow = -150;
  int thresholdCutDiffHigh = 80;
  int noiseCutSynLow = -40;
  int noiseCutSynHigh = 40;
  int noiseCutLinLow = -25;
  int noiseCutLinHigh = 25;
  int noiseCutDiffLow = -200;
  int noiseCutDiffHigh = 200;

  // shift plots
  double shiftLower = -1000.; // to make the shift plot easier to change
  double shiftUpper = 1000.;
  TH1F* synThresholdShift = new TH1F("SynThresholdShift", "Threshold Shift of Syn flavor", 100, shiftLower, shiftUpper);
  TH1F* linThresholdShift = new TH1F("LinThresholdShift", "Threshold Shift of Lin flavor", 100, shiftLower, shiftUpper);
  TH1F* diffThresholdShift = new TH1F("DiffThresholdShift", "Threshold Shift of Diff flavor", 100, shiftLower, shiftUpper);
  THStack* shiftThresholdStack = new THStack("shiftThresholdStack","Threshold, Red: Diff, Green: Lin, Blue: Sync");

  TH1F* synNoiseShift = new TH1F("SynNoiseShift", "Threhsold Shift of Syn flavor", 100, shiftLower, shiftUpper);
  TH1F* linNoiseShift = new TH1F("LinNoiseShift", "Noise Shift of Lin flavor", 100, shiftLower, shiftUpper);
  TH1F* diffNoiseShift = new TH1F("DiffNoiseShift", "Noise Shift of Diff flavor", 100, shiftLower, shiftUpper);
  THStack* shiftNoiseStack = new THStack("shiftNoiseStack","Noise, Red: Diff, Green: Lin, Blue: Sync");

  // connection plots
  TH2F* pixelStatusMapThreshold = new TH2F("PixelStatusMapThreshold", "Threshold/Noise Shift Connection Map", 400, -0.5, 399.5, 192, -0.5, 191.5);

  // get data - update this to manage TFiles and names  better
  TFile* fSynLo = new TFile(synLoFile.c_str());
  TDirectoryFile* synLoData = (TDirectoryFile*) fSynLo->Get((chip+";1").c_str());
  TDirectoryFile* synLoThresholdData = (TDirectoryFile*) synLoData->Get("ThresholdMap_0;1");
  TH2F* loThresholdSyn = (TH2F*) synLoThresholdData->Get("Map_Syn;1");
  TDirectoryFile* synLoNoiseData = (TDirectoryFile*) synLoData->Get("NoiseMap_0;1");
  TH2F* loNoiseSyn = (TH2F*) synLoNoiseData->Get("Map_Syn;1");

  TFile* fLinLo = new TFile(linLoFile.c_str());
  TDirectoryFile* linLoData = (TDirectoryFile*) fLinLo->Get((chip+";1").c_str());
  TDirectoryFile* linLoThresholdData = (TDirectoryFile*) linLoData->Get("ThresholdMap_0;1");
  TH2F* loThresholdLin = (TH2F*) linLoThresholdData->Get("Map_Lin;1");
  TDirectoryFile* linLoNoiseData = (TDirectoryFile*) linLoData->Get("NoiseMap_0;1");
  TH2F* loNoiseLin = (TH2F*) linLoNoiseData->Get("Map_Lin;1");

  TFile* fDiffLo = new TFile(diffLoFile.c_str());
  TDirectoryFile* diffLoData = (TDirectoryFile*) fDiffLo->Get((chip+";1").c_str());
  TDirectoryFile* diffLoThresholdData = (TDirectoryFile*) diffLoData->Get("ThresholdMap_0;1");
  TH2F* loThresholdDiff = (TH2F*) diffLoThresholdData->Get("Map_Diff;1");
  TDirectoryFile* diffLoNoiseData = (TDirectoryFile*) diffLoData->Get("NoiseMap_0;1");
  TH2F* loNoiseDiff = (TH2F*) diffLoNoiseData->Get("Map_Diff;1");

  TFile* fSynHi = new TFile(synHiFile.c_str());
  TDirectoryFile* synHiData = (TDirectoryFile*) fSynHi->Get((chip+";1").c_str());
  TDirectoryFile* synHiThresholdData = (TDirectoryFile*) synHiData->Get("ThresholdMap_0;1");
  TH2F* hiThresholdSyn = (TH2F*) synHiThresholdData->Get("Map_Syn;1");
  TDirectoryFile* synHiNoiseData = (TDirectoryFile*) synHiData->Get("NoiseMap_0;1");
  TH2F* hiNoiseSyn = (TH2F*) synHiNoiseData->Get("Map_Syn;1");

  TFile* fLinHi = new TFile(linHiFile.c_str());
  TDirectoryFile* linHiData = (TDirectoryFile*) fLinHi->Get((chip+";1").c_str());
  TDirectoryFile* linHiThresholdData = (TDirectoryFile*) linHiData->Get("ThresholdMap_0;1");
  TH2F* hiThresholdLin = (TH2F*) linHiThresholdData->Get("Map_Lin;1");
  TDirectoryFile* linHiNoiseData = (TDirectoryFile*) linHiData->Get("NoiseMap_0;1");
  TH2F* hiNoiseLin = (TH2F*) linHiNoiseData->Get("Map_Lin;1");

  TFile* fDiffHi = new TFile(diffHiFile.c_str());
  TDirectoryFile* diffHiData = (TDirectoryFile*) fDiffHi->Get((chip+";1").c_str());
  TDirectoryFile* diffHiThresholdData = (TDirectoryFile*) diffHiData->Get("ThresholdMap_0;1");
  TH2F* hiThresholdDiff = (TH2F*) diffHiThresholdData->Get("Map_Diff;1");
  TDirectoryFile* diffHiNoiseData = (TDirectoryFile*) diffHiData->Get("NoiseMap_0;1");
  TH2F* hiNoiseDiff = (TH2F*) diffHiNoiseData->Get("Map_Diff;1");

  // Fill shift plots
  hiThresholdSyn -> Add(loThresholdSyn, -1);
  hiThresholdLin -> Add(loThresholdLin, -1);
  hiThresholdDiff -> Add(loThresholdDiff, -1);

  for(int i=1; i<=xrange; i++){
    for(int k=1; k<=yrange;k++){
      if (i <= synBoundary) {
	synThresholdShift -> Fill(hiThresholdSyn->GetBinContent(i,k));
      }
      else if (i <= linBoundary) {
	linThresholdShift -> Fill(hiThresholdLin->GetBinContent(i,k));
      }
      else if (i <= diffBoundary) {
	diffThresholdShift -> Fill(hiThresholdDiff->GetBinContent(i,k));
      }
    }
  }

  hiNoiseSyn -> Add(loNoiseSyn, -1);
  hiNoiseLin -> Add(loNoiseLin, -1);
  hiNoiseDiff -> Add(loNoiseDiff, -1);

  for(int i=1; i<=xrange; i++){
    for(int k=1; k<=yrange;k++){
      if (i <= synBoundary) {
	synNoiseShift -> Fill(hiNoiseSyn->GetBinContent(i,k));
      }
      else if (i <= linBoundary) {
	linNoiseShift -> Fill(hiNoiseLin->GetBinContent(i,k));
      }
      else if (i <= diffBoundary) {
	diffNoiseShift -> Fill(hiNoiseDiff->GetBinContent(i,k));
      }
    }
  }

  // categorize pixels
  for(int i=1; i<=xrange; i++){
    for(int k=1; k<=yrange; k++){
      if (i <= synBoundary) {
	if ((hiThresholdSyn->GetBinContent(i,k) < thresholdCutSynLow) or
	    (hiThresholdSyn->GetBinContent(i,k) > thresholdCutSynHigh) or
	    (hiNoiseSyn->GetBinContent(i,k) < noiseCutSynLow) or
	    (hiNoiseSyn->GetBinContent(i,k) > noiseCutSynHigh)) {
	  pixelStatusMapThreshold->SetBinContent(i,k,1);
	}
	else pixelStatusMapThreshold->SetBinContent(i,k,0.01); 
      }
      else if (i <= linBoundary) {
	if ((hiThresholdLin->GetBinContent(i,k) < thresholdCutLinLow) or
	    (hiThresholdLin->GetBinContent(i,k) > thresholdCutLinHigh) or
	    (hiNoiseLin->GetBinContent(i,k) < noiseCutLinLow) or
	    (hiNoiseLin->GetBinContent(i,k) > noiseCutLinHigh)) {
	  pixelStatusMapThreshold->SetBinContent(i,k,1);
	}
	else pixelStatusMapThreshold->SetBinContent(i,k,0.01);    
      }
      else if (i <= diffBoundary) {
	if ((hiThresholdDiff->GetBinContent(i,k) < thresholdCutDiffLow) or
	    (hiThresholdDiff->GetBinContent(i,k) > thresholdCutDiffHigh) or
	    (hiNoiseDiff->GetBinContent(i,k) < noiseCutDiffLow) or
	    (hiNoiseDiff->GetBinContent(i,k) > noiseCutDiffHigh)) {
	  pixelStatusMapThreshold->SetBinContent(i,k,1);
	}
	else pixelStatusMapThreshold->SetBinContent(i,k,0.01); 
      }
    }
  }

  // deletes everything created between here and the TFile opens
  fSynLo->Close();
  fLinLo->Close();
  fDiffLo->Close();
  fSynHi->Close();
  fLinHi->Close();
  fDiffHi->Close();

  TCanvas *thresholdCanvas = (TCanvas*)gROOT->FindObject("thresholdCanvas");
  if(thresholdCanvas==0) thresholdCanvas = new TCanvas("thresholdCanvas", "Threshold", 1200, 1200);
  thresholdCanvas->Clear();
  thresholdCanvas->Divide(2, 2);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(1);

  TExec *ex1 = new TExec("ex1","thresholdPal();");

  thresholdCanvas->cd(1);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  pixelStatusMapThreshold->Draw("colz");
  ex1->Draw();
  pixelStatusMapThreshold->Draw("colz same");

  thresholdCanvas->cd(3);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  synNoiseShift->SetFillColor(kBlue);
  linNoiseShift->SetFillColor(kGreen);
  diffNoiseShift->SetFillColor(kRed);
  shiftNoiseStack->Add(synNoiseShift);
  shiftNoiseStack->Add(linNoiseShift);
  shiftNoiseStack->Add(diffNoiseShift);
  shiftNoiseStack->Draw("nostack");

  thresholdCanvas->cd(4);
  gPad->SetRightMargin(.15);
  gPad->SetLeftMargin(.15);
  synThresholdShift->SetFillColor(kBlue);
  linThresholdShift->SetFillColor(kGreen);
  diffThresholdShift->SetFillColor(kRed);
  shiftThresholdStack->Add(synThresholdShift);
  shiftThresholdStack->Add(linThresholdShift);
  shiftThresholdStack->Add(diffThresholdShift);
  shiftThresholdStack->Draw("nostack");

  try {
    thresholdCanvas->Print((output_dir+"/"+chip+"_threshold.png").c_str());
  }
  catch (exception e) {
    cout << "Threshold print failed.\n";
  }
  return pixelStatusMapThreshold;
}

void thresholdPal() { // threshold palette
  Int_t thresholdPalette[2];
  thresholdPalette[0] = kMagenta+3;
  thresholdPalette[1] = kOrange;
  gStyle->SetPalette(2,thresholdPalette);
}
