#!/bin/sh
begT=20     # max. temperature
maxT=40     # max. temperature
minT=-45    # min. temperature
delT=5      # temperature steps
delWait=60 # waiting time (sec.)
dwStepWait=60 # waiting time (sec.)
dwWaitSteps=14  # dwell time (waiting for sample to settle) 15-1 min. = 14 steps a 1 min.

for ((myround=0;myround<=3;myround+=1))
do

  echo round ${myround}

  # ramp down to min. temperature
  echo "beginning cool-down"
  mybegT=${begT}
  if [ ${myround} -gt 0 ]
  then
     mybegT=(${maxT}-${delT})
  fi
  for ((actT=${mybegT};actT>=${minT};actT-=${delT}))
  do
     echo ${actT}
     actTc=-15
     if [ ${actT} -lt -10 ]
     then
       actTc=-25
     fi
     echo ${actT}" "${actTc} > /tmp/peltier
     sleep ${delWait}
     while [ ! -f /tmp/peltier_attrg ]
     do
       echo "target not yet reached"
       sleep 5
     done
     rm -f /tmp/peltier_attrg 
  done

  # dwell time
  echo "beginning dwell phase"
  for ((idw=1;idw<=${dwWaitSteps};idw+=1))
  do
    echo "dwell " ${idw} "min."
    sleep ${dwStepWait}
  done

  # ramp up to max. temperature
  echo "beginning warm-up"
  for ((actT=(${minT}+${delT});actT<=${maxT};actT+=${delT}))
  do
     echo ${actT}
     actTc=-10
     if [ ${actT} -gt -10 ]
     then
        actTc=20
     fi
     if [ ${actT} -gt 15 ]
     then
        actTc=30
     fi
     echo ${actT}" "${actTc} > /tmp/peltier
     sleep ${delWait}
  done

  # dwell time
  echo "beginning dwell phase"
  for ((idw=1;idw<=${dwWaitSteps};idw+=1))
  do
    echo "dwell " ${idw} "min."
    sleep ${dwStepWait}
  done

done

echo "going to start-T of " ${begT}
for ((actT=(${maxT}-${delT});actT>=${begT};actT-=${delT}))
do
   echo ${actT}
   actTc=10
   echo ${actT}" "${actTc} > /tmp/peltier
   sleep ${delWait}
done
