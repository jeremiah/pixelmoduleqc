#! /bin/bash

PIXQC=/work1/pixtests/pixelmoduleqc/
LABREM=/work1/pixtests/pixelmoduleqc/labRemote

SetVSCC=1.8     # LDO voltage for FE
SetISCC=1.2     # current limit on LDO voltage
SleepLV=1       # wait in seconds after turn on

CURDIR=`pwd`

# enable gcc 7
source scl_source enable devtoolset-7

case "$1" in
  on)
        echo "Powering up LV and HV"
        # turn on fan used for cooling inside box  
	${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab33_goe.json -c Vfan set-voltage -- 8.0 1.5
	${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab33_goe.json -c Vfan power-on 
        # turn on RD53A
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab33_goe.json -c V_SCC set-voltage -- ${SetVSCC} ${SetISCC}
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab33_goe.json -c V_SCC power-on
        
        # wait for chip to come up
        sleep ${SleepLV}
        
        # turn on bias
	./pwrHV.sh on
        ;;

  off)
	# ramp down HV
	./pwrHV.sh off
        echo "Powering down LV"
        # turn off RD53A
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab33_goe.json -c V_SCC power-off
        # turn off inside box fan
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab33_goe.json -c Vfan power-off
        ;;

  *)
        echo "*** Usage: pwrSCC {on|off}"
        exit 1

esac

exit 0
