#! /bin/bash

PIXQC=${HOME}/pixelmoduleqc
LABREM=${HOME}/pixelmoduleqc/labRemote
ITKSW=${HOME}/pixelmoduleqc/ITk-DAQ
MYHOST=`hostname -s`
MYFHOST=`hostname`

CURDIR=`pwd`

if [ $# -lt 2 ]; 
then 
  echo "usage: ./scans.sh <conn cfg.> <scan name(s)>"
  echo "      e.g. ./scans.sh GOE_rd53a_DM1.json std_digitalscan.json std_analogscan.json"
  exit 1
fi


# enable gcc 7
source scl_source enable devtoolset-7

# power bdaqboard
##${PIXQC}/scripts/bdaqctrl.sh start

# power chip/sensor
##${PIXQC}/scripts/pwrfe.sh on

# go to ITk DAQ SW location for scans 
cd ${ITKSW}

# process list of scans
resdirs=""
for scan in "${@:2}"
do
  # lab23 is running YARR, others BDAQ
  if [ ${MYHOST} == "lab23" ]
  then
    bin/scanConsole -r configs/controller/specCfg-rd53a.json -c configs/connectivity/${1} -s configs/scans/rd53a/${scan} -p -W
  else
    bin/scanConsole -r configs/controller/bdaqCfg.json -c configs/connectivity/${1} -s configs/scans/rd53a/${scan} -p -W
  fi
  resdirs=${resdirs}" "`readlink -f data/last_scan`
done

# power off chip/sensor
##${PIXQC}/scripts/pwrfe.sh off

# power off bdaqboard
##${PIXQC}/scripts/bdaqctrl.sh stop

# register DCS values for all scans
echo "processing DCS info"
sleep 1
# retieve module SN
modSN=`jq ".module.serialNumber" ../ITk-DAQ/configs/connectivity/GOE_rd53a_DM2.json  | sed 's/"//g'`
#
for resdir in ${resdirs}
do
   bin/dbAccessor -F ${PIXQC}/conf/${MYHOST}_goe.json -n ${modSN} -s ${resdir}/scanLog.json -d ${HOME}/.yarr/localdb/${MYFHOST}_database.json
done

# back to where we started
cd ${CURDIR}
