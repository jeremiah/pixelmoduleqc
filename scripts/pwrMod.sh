#! /bin/bash

case "$1" in
  on)
        # turn on RD53As
        ./pwrLV.sh on
        # turn on bias
	./pwrHV.sh on
        ;;

  off)
	# ramp down HV
	./pwrHV.sh off
        # turn off RD53As
	./pwrLV.sh off
        ;;

  *)
        echo "*** Usage: pwrMod {on|off}"
        exit 1

esac

exit 0
