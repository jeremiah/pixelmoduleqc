#!/bin/sh
begT=20     # max. temperature
maxT=60     # max. temperature
minT=-55    # min. temperature
delT=5      # temperature steps
delWait=60 # waiting time (sec.)
dwStepWait=60 # waiting time (sec.)
dwWaitSteps=14  # dwell time (waiting for sample to settle) 15-1 min. = 14 steps a 1 min.

# ramp down to min. T
echo "beginning cool-down"
mybegT=${begT}
for ((actT=${mybegT};actT>=${minT};actT-=${delT}))
do
   echo ${actT}
   actTc=-20
   if [ ${actT} -lt -10 ]
   then
     actTc=-40
   fi
   echo ${actT} " " ${actTc} > /tmp/peltier
   sleep ${delWait}
   while [ ! -f /tmp/peltier_attrg ]
   do
     echo "target not yet reached"
     sleep 5
   done
   rm -f /tmp/peltier_attrg 
done

# dwell time
echo "beginning dwell phase"
for ((idw=1;idw<=${dwWaitSteps};idw+=1))
do
  echo "dwell " ${idw} "min."
  sleep ${dwStepWait}
done

# ramp up to max. temperature
echo "beginning warm-up"
for ((actT=(${minT}+${delT});actT<=${maxT};actT+=${delT}))
do
   echo ${actT}
   actTc=-10
   if [ ${actT} -gt -10 ]
   then
      actTc=20
   fi
   if [ ${actT} -gt 15 ]
   then
      actTc=40
   fi
   echo ${actT}" "${actTc} > /tmp/peltier
   sleep ${delWait}
done

# dwell time
echo "beginning dwell phase"
for ((idw=1;idw<=${dwWaitSteps};idw+=1))
do
  echo "dwell " ${idw} "min."
  sleep ${dwStepWait}
done

# go back to starting point
echo "going to start-T of " ${begT}
for ((actT=(${maxT}-${delT});actT>=${begT};actT-=${delT}))
do
   echo ${actT}
   actTc=10
   echo ${actT}" "${actTc} > /tmp/peltier
   sleep ${delWait}
done
