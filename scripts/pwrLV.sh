#! /bin/bash

PIXQC=/home/jthomas/pixelmoduleqc/
LABREM=/home/jthomas/pixelmoduleqc/labRemote

SetVSP=1.8    # SP voltage for FE
SetISP=1.5    # current limit on SP voltage
SleepLV=3     # wait in seconds after turn on

CURDIR=`pwd`

# enable gcc 7
source scl_source enable devtoolset-7

case "$1" in
  on)
        echo "Powering up LV"
        # turn on RD53A
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c VSP power-on ${SetVSP} ${SetISP}
        
        # wait for chip to come up
        sleep ${SleepLV}
        ;;

  off)
        echo "Powering down LV"
        # turn off RD53A
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c VSP power-off
        ;;

  *)
        echo "*** Usage: pwrLV {on|off}"
        exit 1

esac

exit 0

# obsolete locking mechanism (hopefully now covered by GPIB code, but let's keep it just in case...
#while [ -e /tmp/use_pwr ]
#do
#  sleep 0.25
#done
#touch /tmp/use_pwr
#${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV set-voltage -- ${volts}
#rm /tmp/use_pwr
