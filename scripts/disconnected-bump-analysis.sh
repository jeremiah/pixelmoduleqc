#! /bin/bash

# a full analysis of disconnected bumps on one chip

# this file performs the final ROOT analysis for one chip on a DCC module
# the tuning and scanning register values are contained in ./crosstalk-routine.sh and ./threshold-routine.sh
# rootfile creation and disconnected bump scans are contained in ./crosstalk-routine.sh and ./threshold-routine.sh
# management of tuning and power is in ./tune-yarr-crosstalk-dcc.sh and ./tune-yarr-threshold-dcc.sh
# the tuning routines are contained in ../ITk-DAQ/tune-rd53a-crosstalk.sh and ../ITk-DAQ/tune-rd53a-threshold.sh

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <chip_name> <rx_line>" >&2
    exit 1
fi

now=$(date +"%j-%H-%M")
echo "Starting scan $now (day - hour - minute) on chip $1"
OUTPUTDIR=~/pixelmoduleqc/ITk-DAQ/data/${1}_disconnected_bump_scan_${now}/ # might be good to use run number?
mkdir $OUTPUTDIR

./crosstalk-routine.sh $1 $2 $OUTPUTDIR
./threshold-routine.sh $1 $2 $OUTPUTDIR

# TODO: improve root saving behavior
# 'root -b' makes the output look worse, but you can use it to avoid root flashing on the screen
root '~/pixelmoduleqc/analysis/disconnectedBumpAnalysisIndividual.cpp("'${OUTPUTDIR}'","'$1'","'${OUTPUTDIR}'")'

echo "Ending scan $now (day - hour - minute) on chip $1"
