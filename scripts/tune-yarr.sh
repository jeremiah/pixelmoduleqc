#! /bin/bash

CURDIR=`pwd`
cd ..
PIXQC=`pwd`

cd ${CURDIR}
LABREM=${PIXQC}/labRemote
ITKSW=${PIXQC}/ITk-DAQ

# <second_target_threshold> is the one you actually want at the end!!!
if [ "$#" -ne 4 ]; then
    echo "Usage: $0 <first_target_threshold> <second_target_threshold> <target_charge> <target_tot>" >&2
    exit 1
fi


# enable gcc 7
source scl_source enable devtoolset-7

# power chip/sensor
${CURDIR}/pwrMod.sh on

# go to ITk-SW location for scans 
cd ${ITKSW}

# call tuning script from ITk-SW
ctrinit=`cat ~/.yarr/runCounter | awk '{$1=$1+1; print}'`

# no localdb write
# ./scripts/tune-rd53a.sh $1 $2 $3 $4 configs/controller/specCfg-rd53a.json configs/connectivity/GOE_rd53a_SCC-setup.json -W
./scripts/tune-rd53a.sh $1 $2 $3 $4 configs/controller/specCfg-rd53a.json configs/connectivity/GOE_rd53a_SCC-setup.json

ctrfin=`cat ~/.yarr/runCounter`

# back to where we started
cd ${CURDIR}

# power off chip/sensor
${CURDIR}/pwrMod.sh off

# register DCS values for all scans
# this directory does not exist
# echo "processing DCS info"
# sleep 1
# resdirs=""
# for i in $(seq ${ctrinit} ${ctrfin})
# do
#   resdirs=${resdirs}" "`ls ~/ITk-SW/data/*${i}*/scanLog.json`
# done
# for resdir in ${resdirs}
# do
#   bin/dbAccessor -E ${PIXQC}/results/dcsinfo.json -s ${resdir}
# done
