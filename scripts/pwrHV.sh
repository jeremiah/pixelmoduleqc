#! /bin/bash

PIXQC=/home/jthomas/pixelmoduleqc/
LABREM=/home/jthomas/pixelmoduleqc/labRemote

SetVHV=-100    # bias voltage after ramping -40 V or higher for real modules
SetIHV=1.e-6  # current limit on bias voltage
StepHV=-10     # bias voltage ramp steps
SleepHV=5      # wait in seconds after ramp step

CURDIR=`pwd`

# enable gcc 7
source scl_source enable devtoolset-7

case "$1" in
  on)
        echo "Powering up HV"
        # turn on bias
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV set-voltage -- ${StepHV} ${SetIHV}
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV power-on
	# ramp up to final voltage
        for ((volts=${StepHV};volts>=${SetVHV};volts+=${StepHV}))
        do
          echo "setting HV to " ${volts}
          ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV set-voltage -- ${volts}
          # echo "HV setpoint is " $(${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV get-voltage)
          # echo "HV measured as " $(${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV meas-voltage)
	  # echo " - - - "
	  sleep ${SleepHV}
	done
        ;;

  off)
        echo "Powering down HV"
	# ramp down HV
        CurrHV=`${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV get-voltage`
        for ((volts=${CurrHV};volts<=${StepHV};volts-=${StepHV}))
        do
          echo "setting HV to " ${volts}
          ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV set-voltage -- ${volts}
	  sleep ${SleepHV}
	done
        # turn off bias
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/lab23_goe.json -c HV power-off
        ;;

  *)
        echo "*** Usage: pwrHV {on|off}"
        exit 1

esac

exit 0
