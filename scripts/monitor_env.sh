#! /bin/bash
ARDUINO=/usr/local/arduino-1.8.13/arduino
CURDIR=`pwd`
cd ..
PIXQC=`pwd`
cd ${CURDIR}
LABREM=${PIXQC}/labRemote
# kill old monitor processes and wait for loop to finish
pkill monitor_env_loop
#env_monitor
sleep 2
# Silke's alternative:
# screen -ls | awk '/\.env\t/ {print strtonum($1)}' | xargs kill -9
# screen -wipe
#
# flash arduino
${ARDUINO} --board arduino:avr:mega:cpu=atmega2560 --port /dev/ttyACM0 --upload ${LABREM}/arduino/devcomuino/devcomuino.ino
# wait for arduino to be ready and clear output
sleep 5
# start new monitors
screen -d -m -S env -t env ./monitor_env_loop.sh
