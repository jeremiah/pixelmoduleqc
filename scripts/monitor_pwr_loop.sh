#! /bin/bash
MYHOST=`hostname -s`
CURDIR=`pwd`
cd ..
PIXQC=`pwd`
cd ${CURDIR}
LABREM=${PIXQC}/labRemote
while :
do
    ${LABREM}/bin/ps_monitor ${PIXQC}/conf/${MYHOST}_goe.json PowerSupplies VSP:V_SCC
    sleep 2
done
