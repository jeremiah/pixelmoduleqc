#! /bin/bash

# perform a full tuning and scanning for disconnected bump threshold scan

if [ "$#" -ne 2 ] && [ "$#" -ne 3 ]; then
    echo "Usage: $0 <chip_name> <rx_line> [optional_rootfile_storage_dir]" >&2
    exit 1
fi

# threshold tuning targets
SYNTARGET=2000
LINTARGET=2000
DIFFTARGET=2000
TARGET_CHARGE=10000
TARGET_TOT=7

# threshold tuning register settings
SYNCIBIASSF=100
SYNCIBIASP1=38
SYNCIBIASP2=10
LINLDAC=50
LINPAINBIAS=30
DIFFPRMP=20

# default BDAQ config settings
DIFFCOMP=1023
DIFFFOL=542
DIFFLCC=20
DIFFLCCEN=0
DIFFPRECOMP=512
DIFFVFF=40
DIFFVTH1=600
LINKRUMCURR=32
LINVTH=415
SYNCVBL=380
SYNCVTH=390

# get chip config
GC=".RD53A.GlobalConfig"

# UNCOMMENT ONE OF THESE 3 blocks TO TUNE!!!
# 1. full BDAQ register config
# jq '.RD53A.GlobalConfig.DiffComp = '${DIFFCOMP}' | .RD53A.GlobalConfig.DiffFol = '${DIFFFOL}' |
# .RD53A.GlobalConfig.DiffLcc = '${DIFFLCC}' | .RD53A.GlobalConfig.DiffLccEn = '${DIFFLCCEN}' |
# .RD53A.GlobalConfig.DiffPrecomp = '${DIFFPRECOMP}' | .RD53A.GlobalConfig.DiffPrmp = '${DIFFPRMP}' |
# .RD53A.GlobalConfig.DiffVff = '${DIFFVFF}' | .RD53A.GlobalConfig.DiffVth1 = '${DIFFVTH1}' |
# .RD53A.GlobalConfig.LinKrumCur = '${LINKRUMCURR}' | .RD53A.GlobalConfig.LinLdac = '${LINLDAC}' |
# .RD53A.GlobalConfig.LinPaInBias = '${LINPAINBIAS}' | .RD53A.GlobalConfig.LinVth = '${LINVTH}' |
# .RD53A.GlobalConfig.SyncIbiasSf = '${SYNCIBIASSF}' | .RD53A.GlobalConfig.SyncIbiasp1 = '${SYNCIBIASP1}' |
# .RD53A.GlobalConfig.SyncIbiasp2 = '${SYNCIBIASP2}' | .RD53A.GlobalConfig.SyncVbl = '${SYNCVBL}' |
# .RD53A.GlobalConfig.SyncVth = '${SYNCVTH}'' ~/pixelmoduleqc/ITk-DAQ/configs/clean_configs/${1}.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/${1}_threshold.json

# 2. just the BDAQ registers for threshold shift analysis with the rest as YARR defaults
# jq '.RD53A.GlobalConfig.SyncIbiasSf = '${SYNCIBIASSF}' | .RD53A.GlobalConfig.SyncIbiasp1 = '${SYNCIBIASP1}' |
# .RD53A.GlobalConfig.SyncIbiasp2 = '${SYNCIBIASP2}' | .RD53A.GlobalConfig.LinLdac = '${LINLDAC}' |
# .RD53A.GlobalConfig.LinPaInBias = '${LINPAINBIAS}' | .RD53A.GlobalConfig.DiffPrmp = '${DIFFPRMP}'' ~/pixelmoduleqc/ITk-DAQ/configs/clean_configs/${1}.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/${1}_threshold.json

# 3. these are the registers that worked for me
jq ''$GC'.SyncIbiasSf = '${SYNCIBIASSF}' | '$GC'.SyncIbiasp1 = '${SYNCIBIASP1}' |
'$GC'.SyncIbiasp2 = '${SYNCIBIASP2}' | '$GC'.DiffPrmp = '${DIFFPRMP}'' \
~/pixelmoduleqc/ITk-DAQ/configs/clean_configs/${1}.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/${1}_threshold.json

# create connectivity file
jq '.chips[0].rx = '${2}'| .chips[0].config = "configs/'${1}'_threshold.json"' \
~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_template.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_threshold.json

# tune chip
./pwrHV.sh off # tune at 0 to increase noise
./tune-yarr-threshold-dcc.sh $SYNTARGET $LINTARGET $DIFFTARGET $TARGET_CHARGE $TARGET_TOT $1 # UNCOMMENT THIS TO TUNE!!!

# scan chip
# max threshold InjVcalDiff - increase if threshold dist is cut off
SYNMAX=1000
LINMAX=1000
DIFFMAX=1000

#threshold scanning register settings
DIFFPRMP=10
LINPAINBIAS=30
SYNCIBIASSF=45
SYNCIBIASP1=38
SYNCIBIASP2=30

# scan at 0 bias - high
./pwrLV.sh on
cd ../ITk-DAQ

jq '.scan.loops[1].config.min = 0 | .scan.loops[1].config.max = '${SYNMAX}' | .scan.prescan.DiffPrmp = '${DIFFPRMP}' |
.scan.prescan.LinPaInBias = '$LINPAINBIAS' | .scan.prescan.SyncIbiasSf = '${SYNCIBIASSF}' |
.scan.prescan.SyncIbiasp1 = '${SYNCIBIASP1}' | .scan.prescan.SyncIbiasp2 = '${SYNCIBIASP2}'' \
~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/syn_thresholdscan.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_syn_thresholdscan.json
~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_threshold.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_syn_thresholdscan.json #-p 1
SYN_HI_THRESHOLD=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

jq '.scan.loops[1].config.max = '${LINMAX}' | .scan.prescan.DiffPrmp = '${DIFFPRMP}' |
.scan.prescan.LinPaInBias = '$LINPAINBIAS' | .scan.prescan.SyncIbiasSf = '${SYNCIBIASSF}' |
.scan.prescan.SyncIbiasp1 = '${SYNCIBIASP1}' | .scan.prescan.SyncIbiasp2 = '${SYNCIBIASP2}'' \
~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/lin_thresholdscan.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_lin_thresholdscan.json
~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_threshold.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_lin_thresholdscan.json #-p 1
LIN_HI_THRESHOLD=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

jq '.scan.loops[1].config.max = '${DIFFMAX}' | .scan.prescan.DiffPrmp = '${DIFFPRMP}' |
.scan.prescan.LinPaInBias = '$LINPAINBIAS' | .scan.prescan.SyncIbiasSf = '${SYNCIBIASSF}' |
.scan.prescan.SyncIbiasp1 = '${SYNCIBIASP1}' | .scan.prescan.SyncIbiasp2 = '${SYNCIBIASP2}'' \
~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/diff_thresholdscan.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_diff_thresholdscan.json
~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_threshold.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_diff_thresholdscan.json #-p 1
DIFF_HI_THRESHOLD=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

# scan at -100 bias (set by HV script) - low
cd ../scripts
./pwrHV.sh on
cd ../ITk-DAQ

~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_threshold.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_syn_thresholdscan.json #-p 1
SYN_LO_THRESHOLD=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_threshold.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_lin_thresholdscan.json #-p 1
LIN_LO_THRESHOLD=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_threshold.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_diff_thresholdscan.json #-p 1
DIFF_LO_THRESHOLD=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

cd ../scripts
./pwrHV.sh off
./pwrLV.sh off

# create root files
PLOTTING_TOOLS=~/yarr_plotting_tools/plotting-tools/bin
${PLOTTING_TOOLS}/plotFromDir -i $SYN_HI_THRESHOLD
${PLOTTING_TOOLS}/plotFromDir -i $LIN_HI_THRESHOLD
${PLOTTING_TOOLS}/plotFromDir -i $DIFF_HI_THRESHOLD
${PLOTTING_TOOLS}/plotFromDir -i $SYN_LO_THRESHOLD
${PLOTTING_TOOLS}/plotFromDir -i $LIN_LO_THRESHOLD
${PLOTTING_TOOLS}/plotFromDir -i $DIFF_LO_THRESHOLD

if [ "$#" -eq 3 ]; then
    cp $SYN_HI_THRESHOLD/rootfile.root $3/syn_hi_threshold.root
    cp $LIN_HI_THRESHOLD/rootfile.root $3/lin_hi_threshold.root
    cp $DIFF_HI_THRESHOLD/rootfile.root $3/diff_hi_threshold.root
    cp $SYN_LO_THRESHOLD/rootfile.root $3/syn_lo_threshold.root
    cp $LIN_LO_THRESHOLD/rootfile.root $3/lin_lo_threshold.root
    cp $DIFF_LO_THRESHOLD/rootfile.root $3/diff_lo_threshold.root
fi

# optional: run threshold analysis
#root '~/pixelmoduleqc/analysis/plotDisconnectedBumpsThreshold.cpp("'${LO_THREHSHOLD}'/rootfile.root","'${HI_THRESHOLD}'/rootfile.root","'$1';1")'
