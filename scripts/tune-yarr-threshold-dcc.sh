#! /bin/bash

# tune for threshold disc bump scan
# tuning routine is in ../ITk-DAQ/scripts/tune-rd53a-threshold.sh

CURDIR=`pwd`
cd ..
PIXQC=`pwd`

cd ${CURDIR}
LABREM=${PIXQC}/labRemote
ITKSW=${PIXQC}/ITk-DAQ

if [ "$#" -ne 6 ]; then
    echo "Usage: $0 <syn_target_threshold> <lin_target_threshold> <diff_target_threshold> <target_charge> <target_tot> <chip_name>" >&2
    exit 1
fi


# enable gcc 7
source scl_source enable devtoolset-7

# power chip/sensor
${CURDIR}/pwrLV.sh on
wait # sometimes LV doesn't get turned on and so everything fails... maybe this will help?

# go to ITk-SW location for scans 
cd ${ITKSW}

# increment run counter
ctrinit=`cat ~/.yarr/runCounter | awk '{$1=$1+1; print}'`

# call tuning routine
./scripts/tune-rd53a-threshold.sh $1 $2 $3 $4 $5 configs/controller/specCfg-rd53a-16x1.json configs/connectivity/GOE_rd53a_${6}_threshold.json

ctrfin=`cat ~/.yarr/runCounter`

# back to where we started
cd ${CURDIR}

# power off chip/sensor
${CURDIR}/pwrLV.sh off
