#! /bin/bash

MYHOST=`hostname -s`
CURDIR=`pwd`
cd ..
PIXQC=`pwd`
cd ${CURDIR}
LABREM=${PIXQC}/labRemote
ITKSW=${PIXQC}/ITk-DAQ
XILINX=`ls -d /opt/Xilinx/Vivado/20??.? | sed '$!d'`
# XILINX=/opt/Xilinx/Vivado/2017.4
echo ${XILINX}
if [ -z ${XILINX} ]
then
  echo "can't find Xilinx installation"
  exit 2
fi

# enable gcc 7
source scl_source enable devtoolset-7

case "$1" in
  start)
        echo "Starting BDAQ"
        # turn on BDAQ (don't specify voltage: ch.3 has it fixed on the device)
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/${MYHOST}_goe.json -c V_BDAQ power-on
        sleep 1 # wait for BDAQ to come up
        bdcur=`${LABREM}/bin/powersupply -e ${PIXQC}/conf/${MYHOST}_goe.json -c V_BDAQ meas-current`
        echo "current of BDAQ:" ${bdcur} A 
        # flash BDAQ FW
	source ${XILINX}/settings64.sh
	cat << EOF > /tmp/Xil_script.tcl 
connect -host localhost -port 3121
targets 1
fpga -file ${ITKSW}/BDAQ53_1LANE_RX640.bit
disconnect
exit
EOF
        hw_server >& /dev/null &
	xsdb /tmp/Xil_script.tcl
	pkill hw_server
	rm -f /tmp/Xil_script.tcl
        sleep 2 # wait for BDAQ to come up
        ping bdaq1 -c 5 >& /dev/null
        #
        if [ $? != 0 ]
        then
        # BDAQ doesn't respond -> turn off BDAQ, stop script
          ${LABREM}/bin/powersupply -e ${PIXQC}/conf/${MYHOST}_goe.json -c V_BDAQ power-off
          echo "failed to start BDAQ!"
          exit 1
        fi
        #
        echo "success"
        ;;

  stop)
        echo "Shutting down BDAQ"
        ${LABREM}/bin/powersupply -e ${PIXQC}/conf/${MYHOST}_goe.json -c V_BDAQ power-off
        ;;

  *)
        echo "*** Usage: bdaqctrl {start|stop}"
        exit 1

esac

exit 
0
