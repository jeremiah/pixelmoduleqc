import labRemote.devcom, math

class PeltierSensors():
    def __init__(self, device_port, hot_pin, cold_pin, refTemp, Rref, Bntc, V, Rdiv, sht85_addr):
        com=labRemote.com.TextSerialCom(device_port)
        com.setTermination('\r\n')
        com.setConfiguration({'baudrate':'B9600','timeout':5})
        com.init()
        self.hotntc=labRemote.devcom.NTCSensor(hot_pin,com,refTemp,Rref,Bntc,Rdiv,V)
        self.coldntc=labRemote.devcom.NTCSensor(cold_pin,com,refTemp,Rref,Bntc,Rdiv,V)
        i2c=labRemote.devcom.I2CDevComuino(int(sht85_addr,16),com)
        self.sht85=labRemote.devcom.SHT85(i2c)

    def read_hot(self):
        self.hotntc.read()
        return self.hotntc.temperature()

    def read_cold(self):
        self.coldntc.read()
        return self.coldntc.temperature()

    def get_dew_point(self):
        self.sht85.read()
        return self.sht85.dewPoint()
