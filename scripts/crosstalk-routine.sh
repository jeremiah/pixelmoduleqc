#! /bin/bash

# perform a full tuning and scanning for disconnected bump crosstalk scan
# default registers and HV = 0

if [ "$#" -ne 2 ] && [ "$#" -ne 3 ]; then
    echo "Usage: $0 <chip_name> <rx_line> [optional_rootfile_storage_dir]" >&2
    exit 1
fi

# tuning targets
SYNTARGET=2500
LINTARGET=2000
DIFFTARGET=1500
TARGET_CHARGE=10000
TARGET_TOT=7

# default BDAQ registers
# BDAQ scanning changes are in 'goe_discbumpscan.json'
# comment out below and change jq to cp to use default YARR registers for tuning
DIFFCOMP=1023
DIFFFOL=542
DIFFLCC=20
DIFFLCCEN=0
DIFFPRECOMP=512
DIFFPRMP=511
DIFFVFF=40
DIFFVTH1=600
LINKRUMCURR=32
LINLDAC=130
LINPAINBIAS=350
LINVTH=415
SYNCIBIASSF=55
SYNCIBIASP1=90
SYNCIBIASP2=140
SYNCVBL=380
SYNCVTH=390

# get chip config
GC=".RD53A.GlobalConfig"
#UNCOMMENT THIS TO TUNE!!!
jq ''$GC'.DiffComp = '${DIFFCOMP}' | '$GC'.DiffFol = '${DIFFFOL}' | '$GC'.DiffLcc = '${DIFFLCC}' |
'$GC'.DiffLccEn = '${DIFFLCCEN}' | '$GC'.DiffPrecomp = '${DIFFPRECOMP}' | '$GC'.DiffPrmp = '${DIFFPRMP}' |
'$GC'.DiffVff = '${DIFFVFF}' | '$GC'.DiffVth1 = '${DIFFVTH1}' | '$GC'.LinKrumCur = '${LINKRUMCURR}' |
'$GC'.LinLdac = '${LINLDAC}' | '$GC'.LinPaInBias = '${LINPAINBIAS}' | '$GC'.LinVth = '${LINVTH}' |
'$GC'.SyncIbiasSf = '${SYNCIBIASSF}' | '$GC'.SyncIbiasp1 = '${SYNCIBIASP1}' |
'$GC'.SyncIbiasp2 = '${SYNCIBIASP2}' | '$GC'.SyncVbl = '${SYNCVBL}' | '$GC'.SyncVth = '${SYNCVTH}'' \
~/pixelmoduleqc/ITk-DAQ/configs/clean_configs/${1}.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/${1}_crosstalk.json

# Create connectivity file
jq '.chips[0].rx = '${2}'| .chips[0].config = "configs/'${1}'_crosstalk.json"' \
~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_template.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_crosstalk.json

# tune chip
#./pwrMod.sh off # uncomment this if there's a chance that the HV will be on
./tune-yarr-crosstalk-dcc.sh $SYNTARGET $LINTARGET $DIFFTARGET $TARGET_CHARGE $TARGET_TOT $1 # UNCOMMENT THIS TO TUNE!!!

# scan chip and generate root files
# at 0 bias
./pwrLV.sh on
cd ../ITk-DAQ

jq '.scan.loops[0].config.includedPixels = 2' ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan_individual.json 
~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_crosstalk.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan_individual.json #-p 1
SYN_CROSSTALK=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

jq '.scan.loops[0].config.includedPixels = 3'  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan_individual.json
~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_crosstalk.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan_individual.json #-p 1
LIN_CROSSTALK=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

jq '.scan.loops[0].config.includedPixels = 4'  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan_individual.json
~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_crosstalk.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan_individual.json #-p 1
DIFF_CROSSTALK=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`

jq '.scan.loops[0].config.includedPixels = 0'  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan.json | cat > ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan_individual.json
~/pixelmoduleqc/ITk-DAQ/bin/scanConsole -c  ~/pixelmoduleqc/ITk-DAQ/configs/connectivity/GOE_rd53a_${1}_crosstalk.json -r  ~/pixelmoduleqc/ITk-DAQ/configs/controller/specCfg-rd53a-16x1.json -s  ~/pixelmoduleqc/ITk-DAQ/configs/scans/rd53a/goe_discbumpscan_individual.json #-p 1
ALL_CROSSTALK=`readlink -f ~/pixelmoduleqc/ITk-DAQ/data/last_scan`
cd ../scripts
./pwrLV.sh off

# create crosstalk root files
PLOTTING_TOOLS=~/yarr_plotting_tools/plotting-tools/bin
${PLOTTING_TOOLS}/plotFromDir -i $SYN_CROSSTALK
${PLOTTING_TOOLS}/plotFromDir -i $LIN_CROSSTALK
${PLOTTING_TOOLS}/plotFromDir -i $DIFF_CROSSTALK
${PLOTTING_TOOLS}/plotFromDir -i $ALL_CROSSTALK

if [ "$#" -eq 3 ]; then
    cp $SYN_CROSSTALK/rootfile.root $3/syn_crosstalk.root
    cp $LIN_CROSSTALK/rootfile.root $3/lin_crosstalk.root
    cp $DIFF_CROSSTALK/rootfile.root $3/diff_crosstalk.root
    cp $ALL_CROSSTALK/rootfile.root $3/all_crosstalk.root    
fi

# optional: run crosstalk analysis
#root '~/pixelmoduleqc/analysis/plotDisconnectedBumpsCrosstalk.cpp("'${ALL_CROSSTALK}'/rootfile.root","'$1';1")'

#root '~/pixelmoduleqc/analysis/plotDisconnectedBumpsIndividual.cpp("'${SYN_CROSSTALK}'/rootfile.root","'${LIN_CROSSTALK}'/rootfile.root","'${DIFF_CROSSTALK}'/rootfile.root","'$1';1")'
