#! /bin/bash

MYHOST=`hostname -s`

# create set-up specific file for labRemote unless there is one already
if [ ! -f ../conf/${MYHOST}_goe.json ]; then
  cp ../conf/HOST_goe.json ../conf/${MYHOST}_goe.json
fi

# patch db tools
#cp influxdbtool-retrieve ../ITk-DAQ/localdb/bin
cd ../ITk-DAQ
git checkout -- localdb/bin/influxdbtool-retrieve
patch -p 1 < ../scripts/influxdbtool-retrieve_patch.txt
cd ../scripts

# copy set-up specific files to DAQ SW
#cp influxdbtool-retrieve ../ITk-DAQ/localdb/bin
#cp ../conf/GOE_rd53a_quad-setup.json ../ITk-DAQ/configs/connectivity
cp ../conf/BDAQ53_1LANE_RX640.bit ../ITk-DAQ

# modify IV scan json files to reflect PC's config file unless they exist
if [ ! -f lv_ivcurvescan.json ]; then
  cp lv_ivcurvescan.json.tmpl lv_ivcurvescan.json
  sed -i 's/HOST/'${MYHOST}'/g' lv_ivcurvescan.json
fi
if [ ! -f hv_ivcurvescan.json ]; then
  cp hv_ivcurvescan.json.tmpl hv_ivcurvescan.json
  sed -i 's/HOST/'${MYHOST}'/g' hv_ivcurvescan.json
fi
