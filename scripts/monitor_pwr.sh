#! /bin/bash
MYHOST=`hostname -s`
CURDIR=`pwd`
cd ..
PIXQC=`pwd`
cd ${CURDIR}
LABREM=${PIXQC}/labRemote
# kill old monitor processes and wait for loop to finish
pkill monitor_pwr_loop
sleep 2
#
# start new monitors
screen -d -m -S pwr -t pwr ./monitor_pwr_loop.sh
