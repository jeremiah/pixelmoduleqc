#! /bin/bash
ARDUINO=/usr/local/arduino-1.8.13/arduino
MYHOST=`hostname -s`
CURDIR=`pwd`
cd ..
PIXQC=`pwd`
cd ${CURDIR}
LABREM=${PIXQC}/labRemote
#
while :
do
    ${LABREM}/bin/env_monitor -s Climate -c ${PIXQC}/conf/${MYHOST}_goe.json -n Ambient:Module
    sleep 2
done
