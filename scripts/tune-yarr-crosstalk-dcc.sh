#! /bin/bash

CURDIR=`pwd`
cd ..
PIXQC=`pwd`

cd ${CURDIR}
LABREM=${PIXQC}/labRemote
ITKSW=${PIXQC}/ITk-DAQ

# <second_target_threshold> is the one you actually want at the end!!!
if [ "$#" -ne 6 ]; then
    echo "Usage: $0 <syn_target_threshold> <lin_target_threshold> <diff_target_threshold> <target_charge> <target_tot> <chip_name>" >&2
    exit 1
fi


# enable gcc 7
source scl_source enable devtoolset-7

# power chip/sensor
${CURDIR}/pwrLV.sh on
wait # sometimes LV doesn't get turned on and so everything fails... maybe this will help?

# go to ITk-SW location for scans 
cd ${ITKSW}

# call tuning script from ITk-SW
ctrinit=`cat ~/.yarr/runCounter | awk '{$1=$1+1; print}'`

# no localdb write
./scripts/tune-rd53a-crosstalk.sh $1 $2 $3 $4 $5 configs/controller/specCfg-rd53a-16x1.json configs/connectivity/GOE_rd53a_${6}_crosstalk.json

ctrfin=`cat ~/.yarr/runCounter`

# back to where we started
cd ${CURDIR}

# power off chip/sensor
${CURDIR}/pwrLV.sh off
