#! /bin/bash

# tune for crosstalk disc bump scan
# tuning routine is in ../ITk-DAQ/scripts/tune-rd53a-crosstalk.sh

CURDIR=`pwd`
cd ..
PIXQC=`pwd`

cd ${CURDIR}
LABREM=${PIXQC}/labRemote
ITKSW=${PIXQC}/ITk-DAQ

if [ "$#" -ne 6 ]; then
    echo "Usage: $0 <first_target_threshold> <syn_target_threshold> <lin_target_threshold> <diff_target_threshold> <target_charge> <target_tot>" >&2
    exit 1
fi


# enable gcc 7
source scl_source enable devtoolset-7

# power chip/sensor
${CURDIR}/pwrLV.sh on

# go to ITk-SW location for scans 
cd ${ITKSW}

# increment run counter
ctrinit=`cat ~/.yarr/runCounter | awk '{$1=$1+1; print}'`

# call tuning routine
./scripts/tune-rd53a-crosstalk.sh $1 $2 $3 $4 $5 $6 configs/controller/specCfg-rd53a.json configs/connectivity/GOE_rd53a_SCC-setup.json

ctrfin=`cat ~/.yarr/runCounter`

# back to where we started
cd ${CURDIR}

# power off chip/sensor
${CURDIR}/pwrLV.sh off
