#!/bin/bash
# tuning for the threshold disconnected bump test
# less printing for faster tuning

if [ "$#" -lt 7 ]; then
    echo "Usage: $0 <syn_target_threshold> <lin_target_threshold> <diff_target_threshold> <target_charge> <target_tot> <controller_config.json> <config_file.json> [<config_file2.json> ..] " >&2
    exit 1
fi

./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/std_digitalscan.json -p -m 1
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/std_analogscan.json -p

# Tune diff FE
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_tune_globalthreshold.json -t $3
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_tune_pixelthreshold.json -t $3
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_tune_globalpreamp.json -t $4 $5
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_tune_pixelthreshold.json -t $3
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_tune_finepixelthreshold.json -t $3
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_digitalscan.json -p -m 1
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_analogscan.json -p
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_thresholdscan.json -p


# Tune lin FE
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_tune_globalthreshold.json -t $2
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_tune_pixelthreshold.json -t $2
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_tune_globalpreamp.json -t $4 $5
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_retune_pixelthreshold.json -t $2
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_tune_finepixelthreshold.json -t $2
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_digitalscan.json -p -m 1
#/bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_analogscan.json -p
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_thresholdscan.json -p
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_thresholdscan.json -p

# Tune sync FE
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_tune_globalthreshold.json -t $1
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_tune_globalpreamp.json -t $4 $5
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_tune_globalthreshold.json -t $1
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_digitalscan.json -p -m 1
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_analogscan.json -p
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_thresholdscan.json -p

# Final individual scans      
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_digitalscan.json -p -m 1
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_analogscan.json -p
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_thresholdscan.json -p
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/diff_totscan.json -p -t $4
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_digitalscan.json -p -m 1
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_analogscan.json -p
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_thresholdscan.json -p
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/lin_totscan.json -p -t $4
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_digitalscan.json -p -m 1
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_analogscan.json -p
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_thresholdscan.json -p
# ./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/syn_totscan.json -p -t $4

# Finals threshold & tot scans
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/std_thresholdscan.json -p
#./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/std_totscan.json -p -t $4

# Create final mask for operation
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/std_digitalscan.json -p -m 1
./bin/scanConsole -r $6 -c ${@:7} -s configs/scans/rd53a/std_analogscan.json -p

